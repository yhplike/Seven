﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

using Seven.Tools.Extension;

namespace Seven.Member.Web
{
    /// <summary>
    /// 提供一组用于快速获取身份验证服务配置的 API。
    /// </summary>
    public static class SEVENMemberWebConfig
    {
        private static bool? _developerModel;
        private static bool? _friendlyHttpError;
        private static string _loginUrl;

        /// <summary>
        /// 获取 Web.config/App.config 中 appSettings 配置集合下的 key: "SEVENMember:developerModel" 配置项（注意大小写）的值。
        /// 表示是否设定为开发者模式；在开发者模式中，将不会强制执行未登录身份的拦截操作。
        /// 如果 appSettings 中未设定该参数，则取默认值 false。
        /// </summary>
        public static bool DeveloperModel
        {
            get
            {
                if (!_developerModel.HasValue)
                {
                    string value = ConfigurationManager.AppSettings["SEVENMember:developerModel"];
                    _developerModel = value.ToBoolean(false);
                }
                return _developerModel.Value;
            }
        }

        /// <summary>
        /// 获取 Web.config/App.config 中 appSettings 配置集合下的 key: "SEVENMember:friendlyHttpError" 配置项（注意大小写）的值。
        /// 表示当身份验证过滤器或权限验证过滤器验证请求不通过时，是否显示友好的 HTTP 错误消息；如果 appSettings 中未设定该参数，则取默认值 false。
        /// </summary>
        public static bool FriendlyHttpError
        {
            get
            {
                if (!_friendlyHttpError.HasValue)
                {
                    string value = ConfigurationManager.AppSettings["SEVENMember:friendlyHttpError"];
                    _friendlyHttpError = value.ToBoolean(false);
                }
                return _friendlyHttpError.Value;
            }
        }

        /// <summary>
        /// 获取 Web.config/App.config 中 appSettings 配置集合下的 key: "SEVENMember:loginUrl" 配置项（注意大小写）的值。
        /// 表示当身份验证过滤器或权限验证过滤器验证请求不通过时跳转到的登录页面地址；如果 appSettings 中未设定该参数，则取默认值 "~/Account/Login"。
        /// </summary>
        public static string LoginUrl
        {
            get
            {
                if (_loginUrl == null)
                {
                    string value = ConfigurationManager.AppSettings["SEVENMember:loginUrl"];
                    _loginUrl = !string.IsNullOrWhiteSpace(value) ? value : "~/Account/Login";
                }
                return _loginUrl;
            }
        }
    }
}
