﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysUser")]
    public class SysUser : EntityBase<int>
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Display(Name = "用户名")]
        [Required]
        [StringLength(20)]
        public string UserName { get; set; }

        /// <summary>
        /// 登录别名
        /// </summary>
        [Display(Name = "登录别名")]
        public string LoginCode { get; set; }

        /// <summary>
        /// 对应员工ID
        /// </summary>
        [Display(Name = "对应员工ID")]
        public int StaffID { get; set; }

        public virtual HrStaff Staff { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Display(Name = "密码")]
        [Required]
        public string PassWord { get; set; }

        /// <summary>
        /// 对应角色ID
        /// </summary>
        [Display(Name = "对应角色ID")]
        public int RoleID { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        [Display(Name = "是否禁用")]
        public bool IsDisabled { get; set; }

        /// <summary>
        /// 禁用人
        /// </summary>
        [Display(Name = "禁用人")]
        public string Disableder { get; set; }

        /// <summary>
        /// 禁用时间
        /// </summary>
        [Display(Name = "禁用时间")]
        public DateTime? DisabledDate { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [Display(Name = "是否删除")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 删除人
        /// </summary>
        [Display(Name = "删除人")]
        public string Deleteder { get; set; }

        /// <summary>
        /// 删除时间
        /// </summary>
        [Display(Name = "删除时间")]
        public DateTime? DeletedDate { get; set; }
    }
}
