﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysRoleMenuPermission")]
    public class SysRoleMenuPermission : EntityBase<int>
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        [Display(Name = "角色ID")]
        public int RoleID { get; set; }

        /// <summary>
        /// 菜单权限ID
        /// </summary>
        [Display(Name = "菜单权限ID")]
        public int MenuPermissionID { get; set; }

        /// <summary>
        /// 选中状态
        /// </summary>
        [Display(Name = "选中状态")]
        public bool Check { get; set; }
    }
}
