﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysDeskModule")]
    public class SysDeskModule : EntityBase<int>
    {
        /// <summary>
        /// 模块名称
        /// </summary>
        [Display(Name = "模块名称")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 默认位置
        /// </summary>
        [Display(Name = "默认位置")]
        public DeskModuleLocation Location { get; set; }

        /// <summary>
        /// 默认显示条数
        /// </summary>
        [Display(Name = "默认显示条数")]
        public int Size { get; set; }

        /// <summary>
        /// 默认高度
        /// </summary>
        [Display(Name = "高度")]
        public int Height { get; set; }

        /// <summary>
        /// 是否接收推送的消息
        /// </summary>
        [Display(Name = "是否接收推送的消息")]
        public bool ReceiveMessage { get; set; }

        /// <summary>
        /// 控制类型，可选 or 必选
        /// </summary>
        [Display(Name = "控制类型")]
        public DeskModuleControlType ControlType { get; set; }

        /// <summary>
        /// 排序编号
        /// </summary>
        [Display(Name = "排序编号")]
        public int SortNumber { get; set; }

        /// <summary>
        /// 关联菜单
        /// </summary>
        [Display(Name = "关联菜单")]
        public int MenuID { get; set; }

        /// <summary>
        /// Action名称
        /// </summary>
        [Display(Name = "Action名称")]
        public string ActionName { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        [Display(Name = "是否禁用")]
        public bool IsDisabled { get; set; }

        /// <summary>
        /// 是否权限限制
        /// </summary>
        [Display(Name = "是否权限限制")]
        public bool IsLimited { get; set; }
    }
}
