﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysUserBaseConfig")]
    public class SysUserBaseConfig : EntityBase<int>
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        [Display(Name = "用户ID")]
        public int UserID { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public virtual SysUser User { get; set; }

        /// <summary>
        /// 上次使用的主菜单
        /// </summary>
        [Display(Name = "上次使用的主菜单")]
        public int DefaultRootMenuID { get; set; }

        /// <summary>
        /// 上次使用的主题
        /// </summary>
        [Display(Name = "上次使用的主题")]
        public string DefaultThemeName { get; set; }

        /// <summary>
        /// 首页北部panel是否折叠
        /// </summary>
        [Display(Name = "首页北部panel是否折叠")]
        public bool NorthCollapse { get; set; }

        /// <summary>
        /// 首页西部panel是否折叠
        /// </summary>
        [Display(Name = "首页西部panel是否折叠")]
        public bool WestCollapse { get; set; }

        /// <summary>
        /// 首页南部panel是否折叠
        /// </summary>
        [Display(Name = "首页南部panel是否折叠")]
        public bool SouthCollapse { get; set; }
    }
}
