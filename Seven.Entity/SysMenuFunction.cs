﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysMenuFunction")]
    public class SysMenuFunction : EntityBase<int>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Display(Name = "类型")]
        public FunctionType Type { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        [Display(Name = "排序号")]
        public int SortNumber { get; set; }

        /// <summary>
        /// 菜单ID
        /// </summary>
        [Display(Name = "菜单ID")]
        public int MenuID { get; set; }

        /// <summary>
        /// 菜单
        /// </summary>
        [Display(Name = "菜单")]
        //[ForeignKey("MenuID")]
        public virtual SysMenu Menu { get; set; }

        /// <summary>
        /// 动作名称
        /// </summary>
        [Display(Name = "动作名称")]
        [Required]
        public string ActionName { get; set; }

        /// <summary>
        /// 动作说明
        /// </summary>
        [Display(Name = "动作说明")]
        public string ActionDescription { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        [Display(Name = "是否显示")]
        public bool IsShow { get; set; }
    }
}
