﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysUserDeskModule")]
    public class SysUserDeskModule : EntityBase<int>
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        [Display(Name = "用户ID")]
        public int UserID { get; set; }

        /// <summary>
        /// 桌面模块ID
        /// </summary>
        [Display(Name = "桌面模块ID")]
        public int DeskModuleID { get; set; }

        /// <summary>
        /// 默认位置
        /// </summary>
        [Display(Name = "位置")]
        public DeskModuleLocation Location { get; set; }

        /// <summary>
        /// 排序编号
        /// </summary>
        [Display(Name = "排序编号")]
        public int SortNumber { get; set; }

        /// <summary>
        /// 默认显示条数
        /// </summary>
        [Display(Name = "显示条数")]
        public int Size { get; set; }

        /// <summary>
        /// 默认高度
        /// </summary>
        [Display(Name = "高度")]
        public int Height { get; set; }

        /// <summary>
        /// 是否接收推送的消息
        /// </summary>
        [Display(Name = "是否接收推送的消息")]
        public bool ReceiveMessage { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        [Display(Name = "是否显示")]
        public bool IsShow { get; set; }

        /// <summary>
        /// 是否禁用，用来表示对该桌面模块的使用权限是否禁止。这样可以实现取消权限后再开启权限，原用户设置不变。
        /// </summary>
        [Display(Name = "是否禁用")]
        public bool IsDisabled { get; set; }
    }
}
