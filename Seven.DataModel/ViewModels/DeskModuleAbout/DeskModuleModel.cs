﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;

namespace Seven.DataModel.ViewModels.DeskModuleAbout
{
    public class DeskModuleModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 模块名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 默认位置
        /// </summary>
        public DeskModuleLocation Location { get; set; }

        /// <summary>
        /// 默认显示条数
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// 默认高度
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// 是否接收推送的消息
        /// </summary>
        public bool ReceiveMessage { get; set; }

        /// <summary>
        /// 控制类型，可选 or 必选
        /// </summary>
        public DeskModuleControlType ControlType { get; set; }

        /// <summary>
        /// 排序编号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 关联菜单
        /// </summary>
        public int MenuID { get; set; }

        /// <summary>
        /// 关联菜单
        /// </summary>
        public string MenuName { get; set; }

        /// <summary>
        /// Action名称
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// 是否权限限制
        /// </summary>
        public bool IsLimited { get; set; }

        /// <summary>
        /// 是否立即生效。当新增时，true表示立即开放给所有用户；当编辑时，true表示向用户桌面模块设置表中更新。
        /// </summary>
        public bool EffectUser { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
