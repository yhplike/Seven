﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;

namespace Seven.DataModel.ViewModels.MenuAbout
{
    public class MenuFunctionModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }

        // <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public FunctionType Type { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 所属菜单ID
        /// </summary>
        public int MenuID { get; set; }

        /// <summary>
        /// 所属菜单名称
        /// </summary>
        public string MenuName { get; set; }

        /// <summary>
        /// 控制器名称
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// 动作名称
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// 动作说明
        /// </summary>
        public string ActionDescription { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsShow { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
