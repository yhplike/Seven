﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.DataModel.ViewModels.WorkflowAbout
{
    public class FormModel
    {
        /// <summary>
        /// 表单ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 表单名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 所属类型
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 所属类型
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 是否打开设计页面
        /// </summary>
        public bool OpenDesignPage { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
