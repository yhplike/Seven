﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.DataModel.RepositoryModels.UserAbout
{
    public class UserMenuPermissionGridModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// 菜单权限ID
        /// </summary>
        public int MenuPermissionID { get; set; }

        /// <summary>
        /// 选中且打勾，忽略角色权限
        /// 选中不打勾，根据角色权限
        /// 不选中，忽略角色权限
        /// </summary>
        public bool Check { get; set; }

        /// <summary>
        /// 选中且打勾，忽略角色权限
        /// 选中不打勾，根据角色权限
        /// 不选中，忽略角色权限
        /// </summary>
        public bool Indeterminate { get; set; }

        /// <summary>
        /// 菜单权限名称
        /// </summary>
        public string MenuPermissionName { get; set; }

        /// <summary>
        /// 菜单权限排序号
        /// </summary>
        public int MenuPermissionSortNumber { get; set; }

        /// <summary>
        /// 菜单ID
        /// </summary>
        public int MenuID { get; set; }
    }
}
