﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.DataModel.RepositoryModels.WorkflowAbout
{
    public class FormGridModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int ID { get; set; }

        /// 类型名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUserRealName { get; set; }
    }
}
