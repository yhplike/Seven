﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Core;
using Seven.Core.IInitialize;
using Seven.MsSql;

namespace Seven.Service
{
    /// <summary>
    /// 数据工厂
    /// </summary>
    public static class DbFactory
    {
        /// <summary>
        /// 当前数据库类型
        /// </summary>
        private static string DbType = "Entity Framework MsSql";

        /// <summary>
        /// 数据单元
        /// </summary>
        public static IUnitOfWork UnitOfWork
        {
            get
            {
                switch (DbType)
                {
                    case "Entity Framework MsSql": return new MsSqlUnitOfWork();
                    case "ADO.NET Access": return null;
                    case "NHibernate MySql": return null;
                    case "NHibernate Oracle": return null;
                    default: return null;
                }
            }
        }

        /// <summary>
        /// 数据库初始化器
        /// </summary>
        public static IDatabaseInitializer DbInitializer
        {
            get
            {
                switch (DbType)
                {
                    case "Entity Framework MsSql": return new Seven.MsSql.Initialize.DatabaseInitializer();
                    case "ADO.NET Access": return null;
                    case "NHibernate MySql": return null;
                    case "NHibernate Oracle": return null;
                    default: return null;
                }
            }
        }
    }
}
