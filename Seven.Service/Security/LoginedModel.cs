﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Service.Security
{
    /// <summary>
    /// 用户登录身份信息。
    /// </summary>
    public class LoginedModel
    {
        /// <summary>
        /// 表示登录身份信息中的 Token 值。
        /// </summary>
        public Guid Token
        {
            get;
            internal set;
        }

        /// <summary>
        /// 表示登录身份信息中的 用户帐号 值。
        /// </summary>
        public string Account
        {
            get;
            internal set;
        }

        /// <summary>
        /// 表示登录身份信息中的移动设备编码。
        /// </summary>
        public string IMEI
        {
            get;
            internal set;
        }
    }
}
