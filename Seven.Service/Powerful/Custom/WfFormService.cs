﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.WorkflowAbout;
using Seven.DataModel.ViewModels.WorkflowAbout;
using Seven.Tools.Extension;
using Seven.Tools.Helper;

namespace Seven.Service.Powerful
{
    public partial class WfFormService
    {
        #region 获取ViewModel

        /// <summary>
        /// 根据表单类别ID获取新的表单模型
        /// </summary>
        /// <param name="typeId">表单类别ID</param>
        /// <returns></returns>
        public FormModel GetNewModel(int typeId)
        {
            var model = new FormModel();
            model.Type = typeId;
            using (var unit = DbFactory.UnitOfWork)
            {
                model.TypeName = unit.WfFormType.GetFormTypeName(model.Type);
            }
            model.OpenDesignPage = true;

            return model;
        }

        /// <summary>
        /// 根据表单ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">表单类别ID</param>
        /// <returns></returns>
        public FormModel GetEditModel(int id)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                var rep = unit.WfForm;
                var model = rep.GetEditModelByID(id);

                return model;
            }
        }

        /// <summary>
        /// 根据表单ID获取供设计的数据模型
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <returns></returns>
        public FormDesignModel GetDesignModel(int id)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                var rep = unit.WfForm;
                var model = rep.GetDesignModelByID(id);

                return model;
            }
        }

        #endregion

        #region json数据

        /// <summary>
        /// 获取表单列表json数据
        /// </summary>
        /// <param name="typeId">表单类型id</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        public JsonResult FormJson(int typeId, string model, int page, int rows)
        {
            IEnumerable<FormGridModel> data; int total;
            using (var unit = DbFactory.UnitOfWork)
            {
                var rep = unit.WfForm;

                data = rep.FormGrid(typeId, model, page, rows, out total);
            }

            return new DataResult(data).SerializeToGridJsonResult(total);
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存表单信息
        /// </summary>
        /// <param name="model">表单数据模型</param>
        /// <returns></returns>
        public JsonResult SaveForm(FormModel model)
        {
            WfForm entity;
            if (model.ID == 0)
            {
                entity = model.CopyTo<WfForm>();

                using (var unit = DbFactory.UnitOfWork)
                {
                    try
                    {
                        unit.BeginTransaction();
                        unit.WfForm.EntityAdded(entity);
                        var msg = new string[] { "添加成功!", "添加失败!" };
                        var result = new OperationResult(unit.Commit() > 0, msg);
                        if (result.Success)
                        {
                            result.Data = new { id = entity.ID };
                        }
                        return result.SerializeToJsonResult();
                    }
                    catch (DataAccessException ex)
                    {
                        unit.Rollback();
                        throw ExceptionHelper.ThrowServiceException(ex.Message);
                    }
                }
            }
            else
            {
                using (var unit = DbFactory.UnitOfWork)
                {
                    var rep = unit.WfForm;
                    entity = rep.Find(model.ID);
                    entity.Name = model.Name;
                    entity.Type = model.Type;
                    entity.SortNumber = model.SortNumber;
                    entity.Remark = model.Remark;

                    try
                    {
                        unit.BeginTransaction();
                        rep.EntityModified(entity);
                        var msg = new string[] { "更新成功!", "更新失败!" };
                        var result = new OperationResult(unit.Commit() > 0, msg);
                        if (result.Success)
                        {
                            result.Data = new { id = entity.ID };
                        }
                        return result.SerializeToJsonResult();
                    }
                    catch (DataAccessException ex)
                    {
                        unit.Rollback();
                        throw ExceptionHelper.ThrowServiceException(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 删除表单
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <returns></returns>
        public JsonResult RemoveForm(int id)
        {
            WfForm entity = this.GetByKey(id);
            if (entity == null) { return new OperationResult(false, true, "表单不存在！").SerializeToJsonResult(); }

            using (var unit = DbFactory.UnitOfWork)
            {
                var rep = unit.WfForm;
                try
                {
                    unit.BeginTransaction();
                    rep.EntityDeleted(entity);
                    var msg = new string[] { "删除成功!", "删除失败!" };
                    return new OperationResult(unit.Commit() > 0, msg).SerializeToJsonResult();
                }
                catch (DataAccessException ex)
                {
                    unit.Rollback();
                    throw ExceptionHelper.ThrowServiceException(ex.Message);
                }
            }
        }

        #endregion
    }
}
