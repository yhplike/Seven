﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Web.IntelligentQuery.Model;

namespace Seven.Web.IntelligentQuery.TransformProviders
{
    public interface ITransformProvider
    {
        bool Match(ConditionItem item, Type type);

        IEnumerable<ConditionItem> Transform(ConditionItem item, Type type);
    }
}
