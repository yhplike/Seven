﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

using Seven.Tools.Extension;

namespace Seven.Member.Common
{
    /// <summary>
    /// 提供一组用于快速获取用户登录和身份授权验证配置的 API。
    /// </summary>
    public static class Config
    {
        private static int? _sessionTimeout;
        private static int? _sessionInterval;
        private static bool? _enableMultiDeciveOnline;

        /// <summary>
        /// 获取 Web.config/App.config 中 appSettings 配置集合下的 key: "SevenAUTH:sessionTimeout" 配置项（注意大小写）的值。
        /// 表示会话状态提供程序终止会话之前各请求之间所允许的时间（以分钟为单位）；如果 appSettings 中未设定该参数，则取默认值 20。
        /// </summary>
        public static int SessionTimeout
        {
            get
            {
                if (!_sessionTimeout.HasValue)
                {
                    string value = ConfigurationManager.AppSettings["SevenAUTH:sessionTimeout"];
                    _sessionTimeout = value.ToInt(20);
                }
                return _sessionTimeout.Value;
            }
        }

        /// <summary>
        /// 获取 Web.config/App.config 中 appSettings 配置集合下的 key: "SevenAUTH:sessionInterval" 配置项（注意大小写）的值。
        /// 表示会话状态提供程序内存缓存与 Redis 服务的交互和自刷新的时间间隔（以分钟为单位）；如果 appSettings 中未设定该参数，则取默认值 1。
        /// </summary>
        public static int SessionInterval
        {
            get
            {
                if (!_sessionInterval.HasValue)
                {
                    string value = ConfigurationManager.AppSettings["SevenAUTH:sessionInterval"];
                    _sessionInterval = value.ToInt(1);
                }
                return _sessionInterval.Value;
            }
        }

        /// <summary>
        /// 获取 Web.config/App.config 中 appSettings 配置集合下的 key: "SevenAUTH:enableMultiDeciveOnline" 配置项（注意大小写）的值。
        /// 表示服务器是否允许一个用户同时登录多台移动设备；如果 appSettings 中未设定该参数，则取默认值 false。
        /// </summary>
        public static bool EnableMultiDeciveOnline
        {
            get
            {
                if (!_enableMultiDeciveOnline.HasValue)
                {
                    string value = ConfigurationManager.AppSettings["SevenAUTH:enableMultiDeciveOnline"];
                    _enableMultiDeciveOnline = value.ToBoolean(false);
                }
                return _enableMultiDeciveOnline.Value;
            }
        }
    }
}
