﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Seven.Member
{
    /// <summary>
    /// 表示用户身份的登录状态。
    /// </summary>
    public enum LoginState
    {
        /// <summary>
        /// 表示用户当前为离线状态（未登录或者已注销登录）。
        /// </summary>
        [Description("用户当前为离线状态（未登录或者已注销登录）")]
        Offline = 0,

        /// <summary>
        /// 表示用户当前为在线状态（已登录）。
        /// </summary>
        [Description("用户当前为在线状态（已登录）")]
        Online = 1,
    }
}
