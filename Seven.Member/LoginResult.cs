﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Seven.Member
{
    /// <summary>
    /// 表示执行用户登录操作的结果。
    /// </summary>
    public enum LoginResult
    {
        /// <summary>
        /// 表示登入操作时出现了一个未识别的错误。
        /// </summary>
        [Description("登入操作时出现了一个未识别的错误。")]
        Error = 0,

        /// <summary>
        /// 表示登入操作时传入的用户帐号（用户编码或登录名）不正确。
        /// </summary>
        [Description("登入操作时传入的用户帐号（用户编码或登录名）不正确。")]
        InvalidAccount = 1,

        /// <summary>
        /// 表示登入操作时传入的用户密码与用户帐号不匹配。
        /// </summary>
        [Description("登入操作时传入的用户密码与用户帐号不匹配。")]
        InvalidPassword = 2,

        /// <summary>
        /// 表示登入操作时对验证码的校验不匹配。
        /// </summary>
        [Description("登入操作时对验证码的校验不匹配。")]
        InvalidCaptcha = 3,

        /// <summary>
        /// 表示登入操作时用户的状态校验不通过（当前用户可能在其他设备已经登入）。
        /// </summary>
        [Description("登入操作时用户的状态校验不通过（已经登入）。")]
        InvalidLoginStatus = 4,

        /// <summary>
        /// 表示设备登录时设备的校验不通过（该设备不存在或尚未注册/激活）。
        /// </summary>
        [Description("设备登录时设备的校验不通过（该设备不存在或尚未注册/激活）。")]
        InvalidDevice = 5,

        /// <summary>
        /// 表示设备登录时设备的状态校验不通过（该设备正在被使用或该设备上已经登入了一个用户）。
        /// </summary>
        [Description("设备登录时设备的状态校验不通过（该设备正在被使用或该设备上已经登入了一个用户）。")]
        InvalidDeviceStatus = 6,

        /// <summary>
        /// 表示登入操作时用户的状态校验不通过（当前用户可能已经被禁用或尚未激活）。
        /// </summary>
        [Description("登入操作时用户的状态校验不通过（当前用户可能已经被禁用或尚未激活）。")]
        InvalidUserStatus = 7,

        /// <summary>
        /// 表示登入操作成功。
        /// </summary>
        [Description("登入操作成功。")]
        Success = 200,
    }
}
