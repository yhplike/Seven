﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using System.ComponentModel;

using Seven.Service;
using Seven.Service.Security;
using Seven.Site.Mvc.Attributes;
using Seven.Site.Mvc.Filters;
using Seven.Site.Models.AccountModel;

namespace Seven.Site.Web.Controllers
{
    public class HomeController : BaseController
    {
        #region 页面

        /// <summary>
        /// 首页页面
        /// </summary>
        /// <returns></returns>
        [Description("首页页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        [Login]
        public ViewResult Index()
        {
            MainPageModel model = new MainPageModel();
            model.Account = CurrentUserBaseInfo.Account;
            model.Token = CurrentUserBaseInfo.Token;
            model.RealName = CurrentUserBaseInfo.RealName;
            model.UserPool = SiteCore.UseRemotePool;
            if (model.UserPool)
            {
                var SC = new SiteCore();
                model.SignalRServiceUrl = SC.SignalRServiceUrl;
                model.SignalRHubName = SC.SignalRHubName;
                model.SiteSignalRGroupName = SC.SiteSignalRGroupName;
            }

            var BaseConfig = new Account().GetUserBaseConfig(CurrentUserBaseInfo.UserID);
            model.North = BaseConfig.NorthCollapse;
            model.West = BaseConfig.WestCollapse;
            model.South = BaseConfig.SouthCollapse;
            model.DefaultThemeName = BaseConfig.DefaultThemeName;
            model.DefaultRootMenuID = BaseConfig.DefaultRootMenuID;
            model.SystemName = "测试系统名称";

            new Cache().AddThemeCookie(CurrentUserBaseInfo.UserName, model.DefaultThemeName);

            return View(model);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存布局位置
        /// </summary>
        /// <param name="region">位置，只能是 north、west、south</param>
        /// <param name="collapse">是否折叠</param>
        [Description("保存布局位置")]
        [Login]
        [CatchException(ActionType.忽略异常)]
        public void SetHomeLayoutState(string region, bool collapse)
        {
            new Account().SaveLayoutState(CurrentUserBaseInfo.UserID, region, collapse);
        }

        /// <summary>
        /// 保存用户默认的主菜单
        /// </summary>
        /// <param name="id">菜单ID</param>
        [Description("保存用户默认的主菜单")]
        [CatchException(ActionType.忽略异常)]
        [Login]
        public void SetDefaultRootMenu(int id)
        {
            new Account().SaveRootMenuID(CurrentUserBaseInfo.UserID, id);
        }

        /// <summary>
        /// 保存用户默认的主题
        /// </summary>
        /// <param name="name">主题名称</param>
        [Description("保存用户默认的主题")]
        [CatchException(ActionType.忽略异常)]
        [Login]
        public void SetDefaultTheme(string name)
        {
            new Account().SaveThemeName(CurrentUserBaseInfo.UserID, name);
        }

        #endregion
    }
}