﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using Seven.Service;
using Seven.Site.Mvc.Attributes;

namespace Seven.Site.Web.Controllers
{
    public class DeskController : BaseController
    {
        #region 页面

        /// <summary>
        /// 桌面主页
        /// </summary>
        /// <returns></returns>
        [Description("桌面主页")]
        [Login]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public ViewResult Index()
        {
            return View(ServiceFactory.SysUserDeskModule.CurrentUserUseableDeskModules());
        }

        #region 桌面模块

        /// <summary>
        /// 测试模块1
        /// </summary>
        /// <returns></returns>
        [Description("测试模块1")]
        [Login]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult Test1()
        {
            throw new Exception("本项测试旨在检测“手动抛出异常后由异常捕获器捕获并根据Action类型自动判定返回视图还是json数据”，再点击“修改密码”试试。");
            return PartialView();
        }

        /// <summary>
        /// 测试模块2
        /// </summary>
        /// <returns></returns>
        [Description("测试模块2")]
        [Login]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult Test2()
        {
            return PartialView();
        }

        /// <summary>
        /// 测试模块3
        /// </summary>
        /// <returns></returns>
        [Description("测试模块3")]
        [Login]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult Test3()
        {
            return PartialView();
        }

        #endregion

        #endregion

        #region 数据

        #endregion

        #region 操作

        #endregion

        #region 其他

        #endregion

    }
}