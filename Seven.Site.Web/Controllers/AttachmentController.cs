﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.IO;

using Seven.Site.Mvc.Attributes;
using Seven.Web.Http;

namespace Seven.Site.Web.Controllers
{
    public class AttachmentController : Controller
    {


        #region 页面

        /// <summary>
        /// 附件上传页面
        /// </summary>
        /// <returns></returns>
        [Description("附件上传页面")]
        [AllowAnonymous]
        [ViewPage]
        public ViewResult FileUploadForm()
        {
            return View();
        }

        #endregion

        #region 数据

        #endregion

        #region 操作

        [Description("附件上传")]
        [AllowAnonymous]
        [HttpPost]
        public JsonResult UploadFile()
        {
            HttpContextBase context = this.HttpContext;
            var files = context.Request.Files; long nextStartPosition = 0;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFileBase file = files[i];
                string savePath = context.Request.MapPath("/Upload");
                string path = Path.Combine(savePath, file.FileName);
                long fileLenth;
                nextStartPosition = new HttpUploadProcess().SaveAs(path, savePath, file, out fileLenth);
                context.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                context.Response.AddHeader("Range", string.Format("bytes={0}-{1}/{2}", 0, nextStartPosition, fileLenth));
                context.Response.Expires = -1;
            }

            return Json(new { next = nextStartPosition });
        }

        //[HttpGet]
        //[AllowAnonymous]
        //public JsonResult UploadFile(string md5)
        //{
        //    bool exist = false;


        //    return Json(new { exist = exist }, JsonRequestBehavior.AllowGet);
        //}

        [Description("处理附件临时文件")]
        [AllowAnonymous]
        public JsonResult DealFileTemp(string fileName, string md5)
        {
            string savePath = this.HttpContext.Request.MapPath("/Upload");
            string path = Path.Combine(savePath, fileName);

            bool success = new HttpUploadProcess().DealTemp(path, md5);

            return Json(new { success = success });
        }

        #endregion

        #region 其他


        #endregion
    }
}