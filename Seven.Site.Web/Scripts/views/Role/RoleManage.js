﻿/*
==============================================================================
//  角色管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Role.RoleManage");

    var tree = "#tree", dg = "#dg", queryFm = "#queryFm";

    window.views.Role.RoleManage.initPage = function (permissions, currentRoleId) {

        var data = window.jeasyui.helper.buildToolbarUsePermissionJson(permissions, true);
        data.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Role.reloadRole(); } } });

        var treeInit = function () {

            var options = {
                url: "/Public/CompanyJson",
                animate: true,
                onBeforeSelect: function (node) {
                    return node.attributes.IsCompany;
                },
                onSelect: function (node) {
                    $(dg).datagrid("load", { companyId: node.id, model: $(queryFm).serialize() });
                },
                onBeforeLoad: function (node, param) {
                    $.easyui.loading({ locale: $(this).currentRegion(), msg: "数据加载中..." });
                },
                onLoadSuccess: function (node) {
                    $.easyui.loaded($(this).currentRegion());
                }
            };

            $(tree).tree(options);
        };

        var bindQueryButtonEvent = function () {
            $("#aquery", queryFm).click(function () {
                var queryParam = { model: $(queryFm).serialize() };
                var selected = $(tree).tree("getSelected");
                if (selected) {
                    if (selected.attributes.IsCompany) { queryParam.companyId = selected.id; }
                };
                $(dg).datagrid('load', queryParam);
            });
        };

        var dataGridInit = function () {
            var toolbar = $("<div />").appendTo("body").toolbar({
                data: data
            }).toolbar("toolbar");

            var frozenColumns = function (showCheckBox) {
                var result = [];
                if (showCheckBox) {
                    result.push({ field: 'ck', checkbox: true });
                }
                var normal = [{ field: 'Name', title: '角色名称', width: 100 }, { field: 'Code', title: '角色编号', width: 100 }];
                $.util.merge(result, normal);
                return result;
            };

            var columns = function (sortableEnable) {
                var result = [];
                var normal = [
                        { field: 'Icon', title: '图标', width: 100 },
                        { field: 'SortNumber', title: '排序号', width: 100 },
                        { field: 'CompanyName', title: '所属公司', width: 100 },
                        {
                            field: 'IsDisabled', title: '是否禁用', width: 120,
                            formatter: function (value) {
                                return value ? "<b>禁用</b>" : "<b>启用</b>"
                            },
                            styler: function (val, row) {
                                if (!val)
                                { return window.comlib.stateColor.green; }
                                else
                                { return window.comlib.stateColor.red; }
                            }
                        },
                        {
                            field: 'operator', title: '操作', width: 150,
                            formatter: function (val, row, index) {
                                var strAction = "";
                                if (row.IsDisabled) {
                                    strAction = window.comlib.formatLinkButton({ "class": "btn-state" }, "icon-ok", "启用");
                                } else {
                                    strAction = window.comlib.formatLinkButton({ "class": "btn-state" }, "icon-ok", "禁用");
                                }
                                return strAction;
                            }
                        }
                ];
                if (sortableEnable) {
                    $.array.forEach(normal, function (val, index, array) {
                        val.sortable = true;
                    });
                }
                $.util.merge(result, normal);
                return result;
            };

            var options = {
                url: "/Role/RoleManageJson",
                queryParams: { first: true, rangeFilter: true },
                idField: "ID",
                toolbar: toolbar,
                rownumbers: false,
                nowrap: true,
                striped: true,
                fit: true,
                border: false,
                remoteSort: false,
                singleSelect: true,
                checkOnSelect: false,
                selectOnCheck: false,
                frozenColumns: [frozenColumns(false)],
                columns: [columns(true)],
                pagination: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                },
                onLoadSuccess: function (data) {
                    $.fn.datagrid.extensions.onLoadSuccess.apply(this, arguments);//调用扩展的事件防止覆盖
                    var t = $(this), col = t.datagrid("getColumnDom", "operator"), rows = t.datagrid("getRows");
                    col.find("a.l-btn.btn-state").each(function (i, ele) {
                        $(this).click(function (e) {
                            if (String(rows[i].ID) == currentRoleId) { $.easyui.messager.alert("不允许禁用自己的角色！"); e.stopPropagation(); }
                            else {
                                window.views.Role.enableOrDisableRole(rows[i].ID, rows[i].IsDisabled);
                                e.stopPropagation();
                            }
                        });
                    });
                }
            };

            $(dg).datagrid(options);
            window.jeasyui.helper.drawTopBorderForDataGridTools($(dg));
        };

        treeInit();
        bindQueryButtonEvent();
        dataGridInit();
    };

    window.views.Role.createRole = function () {
        var selectNode = $(tree).tree("getSelected");
        var param = { companyId: 0, companyName: "" };
        if (selectNode) {
            if (selectNode.attributes.IsCompany) { param.companyId = selectNode.id; param.companyName = selectNode.text }
        }

        $.easyui.showDialog({
            title: "新建角色",
            href: "/Role/RoleCreateForm?" + $.param(param),
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    if (model.CompanyID == 0) { $.easyui.messager.alert("请选择角色的所属公司！"); return false; }
                    window.views.Role.saveRole(model);
                } else { return false; }
            },
            width: 850,
            height: 280
        });
    };

    window.views.Role.editRole = function (row) {
        row = row ? row : $(dg).datagrid("getSelected");
        if (row) {
            $.easyui.showDialog({
                title: "编辑角色 " + row.Name,
                href: "/Role/RoleEditForm?id=" + row.ID,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.Role.saveRole(model);
                    } else { return false; }
                },
                width: 850,
                height: 280
            });
        }
    };

    window.views.Role.saveRole = function (model) {
        $.easyui.loading({ locale: window.top.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Role/SaveRole", model, function (result) {
            $.easyui.loaded(window.top.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Role.reloadRole);
        }, 'json');
    };

    window.views.Role.removeRole = function (row) {
        row = row == undefined ? $(dg).datagrid("getSelected") : row;
        if (row) {
            var _callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Role/RemoveRole", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBackWhenRemove.call(this, result, function () { $(dg).datagrid("deleteRow", $(dg).datagrid("getRowIndex", row)); });
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", _callback);
        }
    };

    window.views.Role.reloadRole = function () {
        $(dg).datagrid('clearSelections');
        //$(dg).datagrid('clearChecked');
        $(dg).datagrid("load");
    };

    window.views.Role.enableOrDisableRole = function (id, state) {
        if (!$.util.isNullOrUndefined(id) && !$.util.isNullOrUndefined(state)) {
            $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
            $.post("/Role/EnableOrDisableRole", { id: id, disableState: state }, function (result) {
                $.easyui.loaded($("body"));
                window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Role.reloadRole);
            });
        }
    };

})(jQuery);