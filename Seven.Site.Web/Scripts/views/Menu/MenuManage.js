﻿/*
==============================================================================
//  菜单管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Menu.MenuManage");

    var tg = "#tg", dg = "#dg";

    window.views.Menu.MenuManage.initPage = function (permissions, menuGroup, menuFunctionGroup) {
        var data = window.jeasyui.helper.buildToolbarUsePermissionJson(permissions, true, true);

        var _treeGridInit = function () {
            var data1 = window.jeasyui.helper.splitToolbar(data, menuGroup);
            data1.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Menu.reloadMenu(); } } });
            var toolbar = $("<div />").appendTo("body").toolbar({
                data: data1
            }).toolbar("toolbar");

            var frozenColumns = function (showCheckBox) {
                var result = [];
                if (showCheckBox) {
                    result.push({ field: 'ck', checkbox: true });
                }
                var normal = [{ field: 'Name', title: '名称', width: 200 }];
                $.util.merge(result, normal);
                return result;
            };

            var columns = function (sortableEnable) {
                var result = [];
                var normal = [
                        { field: 'Code', title: '编号', width: 100 },
                        { field: 'SortNumber', title: '排序号', width: 80 },
                        { field: 'ControllerName', title: '控制器', width: 100 },
                        { field: 'ActionName', title: '动作', width: 100 },
                        { field: 'Url', title: '完整路径', width: 200 },
                        {
                            field: 'IsShow', title: '是否显示', width: 80,
                            formatter: function (val) {
                                return val ? "<b>显示</b>" : "<b>隐藏</b>";
                            },
                            styler: function (val, row) {
                                if (val)
                                { return window.comlib.stateColor.green; }
                                else
                                { return window.comlib.stateColor.red; }
                            }
                        }];
                if (sortableEnable) {
                    $.array.forEach(normal, function (val, index, array) {
                        $.extend(val, { sortable: true });
                    });
                }
                $.util.merge(result, normal);
                return result;
            };

            var options = {
                url: "/Menu/MenuJsonForTreeGrid",
                idField: "ID",
                treeField: "Name",
                parentField: "ParentID",
                toolbar: toolbar,
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                frozenColumns: [frozenColumns(false)],
                columns: [columns(true)],
                onSelect: function (row) {
                    if (!$.string.isNullOrWhiteSpace(row.Url)) {
                        $(dg).datagrid("load", { menuID: row.ID });
                    }
                    else { $(dg).datagrid("loadData", []); }
                }
            };

            $(tg).treegrid(options);
        };

        var _dataGridInit = function () {
            var data1 = window.jeasyui.helper.splitToolbar(data, menuFunctionGroup);
            data1.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Menu.reloadMenuFunction(); } } });
            var toolbar = $("<div />").appendTo("body").toolbar({
                data: data1
            }).toolbar("toolbar");

            var columns = function (sortableEnable) {
                var result = [];
                var normal = [
                    {
                        field: 'FunctionName', title: '名称', width: 120,
                        formatter: function (val, row) {
                            return "<b>" + val + "</b>";
                        }
                    },
                    {
                        field: 'ActionName', title: '动作', width: 150,
                        formatter: function (val, row) {
                            return val;
                        }
                    },
                    {
                        field: 'FunctionType', title: '类型', width: 80,
                        formatter: function (val, row) {
                            return "<b>" + val + "</b>";
                        }
                    }];
                if (sortableEnable) {
                    $.array.forEach(normal, function (val, index, array) {
                        $.extend(val, { sortable: true });
                    });
                }
                $.util.merge(result, normal);
                return result;
            };

            var options = {
                url: "/Menu/MenuFunctionJson",
                title: "菜单功能点管理",
                queryParams: { menuId: 0 },
                idField: "ID",
                toolbar: toolbar,
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                columns: [columns(false)],
                onBeforeLoad: function (param) {
                    return param.menuId != 0;
                }
            };

            $(dg).datagrid(options);
        };

        _treeGridInit();
        _dataGridInit();
    };

    window.views.Menu.MenuPermissionManage = function () {
        var row = $(tg).treegrid("getSelected");
        if (row && !$.string.isNullOrWhiteSpace(row.Url)) {
            $.easyui.showDialog({
                title: row.Name + " - 权限配置",
                href: "/Menu/MenuPermissionManageIndex?menuId=" + row.ID + "&menuName=" + escape(row.Name),
                enableSaveButton: false,
                enableApplyButton: false,
                width: 550,
                height: 480
            });
        }
    };

    window.views.Menu.createMenu = function (row) {
        row = row == undefined ? $(tg).treegrid("getSelected") : row;
        var parentID = row == undefined ? 0 : row.ID;
        $.easyui.showDialog({
            title: "新建菜单",
            href: "/Menu/MenuCreateForm?parentId=" + parentID,
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.Menu.saveMenu(model);
                } else { return false; }
            },
            width: 850,
            height: 340,
            topMost: true
        });
    };

    window.views.Menu.editMenu = function (row) {
        row = row == undefined ? $(tg).treegrid("getSelected") : row;
        if (row) {
            $.easyui.showDialog({
                title: "编辑菜单-" + row.Name,
                href: "/Menu/MenuEditForm?id=" + row.ID,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.Menu.saveMenu(model);
                    } else { return false; }
                },
                width: 850,
                height: 340
            });
        }
    };

    window.views.Menu.saveMenu = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Menu/SaveMenu", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBackForTreeGrid.call(this, result, $(tg));
        }, 'json');
    };

    window.views.Menu.removeMenu = function (row) {
        row = row == undefined ? $("#tg").treegrid("getSelected") : row;
        if (row) {
            var _callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Menu/RemoveMenu", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, function () { $(tg).treegrid("remove", row.ID); $(dg).datagrid("loadData", []); });
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", _callback);
        }
    };

    window.views.Menu.reloadMenu = function () {
        $(tg).treegrid('clearSelections');
        //$(tg).treegrid('clearChecked');
        $(tg).treegrid("load");
    };


    window.views.Menu.createMenuFunction = function (menuId) {
        if (!menuId) {
            var row = $(tg).treegrid("getSelected");
            if (row && !$.string.isNullOrWhiteSpace(row.Url)) {
                menuId = row.ID;
            }
        }
        if (menuId) {
            $.easyui.showDialog({
                title: "新建菜单权限",
                href: "/Menu/MenuFunctionCreateForm?menuId=" + menuId,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.Menu.saveMenuFunction(model);
                    } else { return false; }
                },
                width: 850,
                height: 340,
                topMost: true
            });
        }
    };

    window.views.Menu.editMenuFunction = function (row) {
        row = row == undefined ? $(dg).datagrid("getSelected") : row;
        if (row) {
            $.easyui.showDialog({
                title: "编辑菜单权限-" + row.FunctionName,
                href: "/Menu/MenuFunctionEditForm?id=" + row.ID,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.Menu.saveMenuFunction(model);
                    } else { return false; }
                },
                width: 850,
                height: 340,
                topMost: true
            });
        }
    };

    window.views.Menu.saveMenuFunction = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Menu/SaveMenuFunction", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Menu.reloadMenuFunction);
        }, 'json');
    };

    window.views.Menu.removeMenuFunction = function (row) {
        row = row == undefined ? $(dg).datagrid("getSelected") : row;
        if (row) {
            var _callback = function () {
                $.easyui.loading({ locale: window.$("body"), msg: "数据处理中，请稍等..." });
                $.post("/Menu/RemoveMenuFunction", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Menu.reloadMenuFunction);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.FunctionName + " 吗?", _callback);
        }
    };

    window.views.Menu.reloadMenuFunction = function () {
        $(dg).datagrid('clearSelections');
        //$(dg).datagrid('clearChecked');
        $(dg).datagrid("load");
    };

})(jQuery);