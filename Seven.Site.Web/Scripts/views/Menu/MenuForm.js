﻿/*
==============================================================================
//  菜单新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Menu.MenuForm");

    window.views.Menu.MenuForm.initPage = function (form) {

        var _form = $("#" + form);

        var _key = $("#ID", _form).val();

        var _controlInit = function () {
            $("#Name", _form).validatebox({
                required: true
            });

            $("#SortNumber", _form).numberbox({
                required: true,
                validType: ['length[1,2]']
            });

            $("#ActionName", _form).combobox({
                valueField: "id",
                textField: "text",
                width: 200,
                editable: false,
                onChange: function (newValue, oldValue) {
                    var controller = $("#ControllerName", _form).combobox("getValue");
                    if (!$.string.isNullOrWhiteSpace(controller)) {
                        $("span#url", _form).text("/" + controller + "/" + newValue);
                    }
                    else { $("span#url", _form).text(""); }
                },
                onLoadSuccess: function () {
                    var defaultValue = $(this).combobox("getValue");
                    if ($.string.isNullOrWhiteSpace(defaultValue)) {
                        var data = $(this).combobox("getData");
                        if (data.length > 0) {
                            $(this).combobox("setValue", data[0].id);
                            var controller = $("#ControllerName", _form).combobox("getValue");
                            $("span#url", _form).text("/" + controller + "/" + data[0].id);
                        }
                        else {
                            var oldText = $("span#url", _form).text();
                            $("span#url", _form).text(oldText.substring(0, oldText.lastIndexOf("/") + 1) + "无效Action");
                        }
                    }
                    else {
                        var data = $(this).combobox("getData");
                        if (data.length) {
                            var record = $.array.first(data, function (item) { return item.id == defaultValue });
                            if (!record) { $(this).combobox("setValue", data[0].id); }
                        }
                        else {
                            $(this).combobox("setValue", '');
                        }
                    }
                }
            });



            $("#ControllerName", _form).combobox({
                url: "/Public/ControllerJson",
                valueField: "id",
                textField: "text",
                width: 200,
                editable: false,
                onChange: function (newValue, oldValue) {
                    actionNameInit(newValue);
                },
                onLoadSuccess: function () {
                    var defaultValue = $(this).combobox("getValue");
                    if (!$.string.isNullOrWhiteSpace(defaultValue)) {
                        var data = $(this).combobox("getData");
                        if (data.length > 0) {
                            actionNameInit(defaultValue);
                        }
                    }
                }
            });

            $("#Remark", _form).attr(window.comlib.textAreaAttr);

            var actionNameInit = function (controller) {
                if (!$.string.isNullOrWhiteSpace(controller)) {
                    $.post("/Public/ActionJson", { controllerName: controller }, function (data) {
                        $("#ActionName", _form).combobox("loadData", data);
                    });
                }
                else {
                    $("#ActionName", _form).combobox("setValue", "");
                    $("#ActionName", _form).combobox("clear");
                }
            };
        };

        var _bindButtonEvent = function () {
            $("#select_parent", _form).click(function () {
                window.comlib.showMenuSelector(function (node) {
                    if (node) {
                        $("#ParentID", _form).val(node.id);
                        $("#ParentID", _form).next().text(node.text);
                        var children = $(this).tree("getNearChildren", node.target), sortNumber = 0;
                        for (var i = 0; i < children.length; i++) {
                            if (Number(children[i].attributes.SortNumber) > sortNumber) {
                                sortNumber = Number(children[i].attributes.SortNumber);
                            }
                        }
                        $("#SortNumber", _form).numberbox("setValue", sortNumber == 99 ? sortNumber : (sortNumber + 1));
                    }
                }, $("#ParentID", _form).val(), _key, true);
            });

            $("#clear_Controller", _form).click(function () {
                $("#ControllerName", _form).combobox("setValue", "");
            });
        };

        _controlInit();
        _bindButtonEvent();
    };

})(jQuery);