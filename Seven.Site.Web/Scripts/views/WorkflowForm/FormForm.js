﻿/*
==============================================================================
//  表单新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.WorkflowForm.FormForm");

    window.views.WorkflowForm.FormForm.initPage = function (form) {

        var _form = $("#" + form);

        var _key = $("#ID", _form).val();

        var _controlInit = function () {
            $("#Name", _form).validatebox({
                required: true
            });

            $("#SortNumber", _form).numberbox({
                required: true
            });

            $("#Remark", _form).attr(window.comlib.textAreaAttr);
        };

        var _bindButtonEvent = function () {
            
        };

        _controlInit();
        _bindButtonEvent();
    };

})(jQuery);