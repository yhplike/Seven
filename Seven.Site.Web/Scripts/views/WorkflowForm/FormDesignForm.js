﻿/*
==============================================================================
//  表单设计 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.WorkflowForm.FormDesignForm");

    window.views.WorkflowForm.FormDesignForm.initPage = function (editor) {

        editor.addContentsCss("");
        var _bindButtonEvent = function () {
            
            $("#conTable").click(function () {
                editor.execCommand("table");
            });

            $("#conTextField").click(function () {
                editor.execCommand("sevenTextfield");
            });

            $("#conTextArea").click(function () {
                editor.execCommand("sevenTextarea");
            });

            $("#conCheckbox").click(function () {
                editor.execCommand("sevenCheckbox");
            });

            $("#conDateTimebox").click(function () {
                editor.execCommand("sevenDatetimebox");
            });

            $("#conMacro").click(function () {
                editor.execCommand("sevenMacro");
            });

            $("#conCalculate").click(function () {
                editor.execCommand("sevenCalculate");
            });

            var _saveDesign = function (closeAfterSave) {
                closeAfterSave = closeAfterSave == undefined ? false : closeAfterSave;
                var content = editor.getData();
                var checkEditor = function () {
                    if ($.string.isNullOrWhiteSpace(content)) {
                        $.messager.alert("表单内容为空，不需要保存。");
                        return false;
                    }
                    return true;
                };

                if (checkEditor() && editor.checkDirty()) {
                    $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                    $.post("/WorkflowForm/SaveFormDesign", { id: 1, content: content }, function (result) {
                        $.easyui.loaded(top.window.$("body"));
                        if (closeAfterSave && result.success) {
                            window.close();
                        }
                    });
                }
            };

            $("#btnSave").click(_saveDesign);

            $("#btnPreview").click(function () {
                editor.execCommand("sevenPreview");
            });

            $("#btnClose").click(function () {
                var msg = '关闭表单设计器前，是否保存对表单的修改？';
                var _callback = function (param) {
                    if (param == undefined) { }
                    else {
                        var result = $.string.toBoolean(String(param));
                        if (result) {
                            _saveDesign(true);
                        }
                        else { window.close(); }
                    }
                };
                window.jeasyui.helper.solicitCallBack(msg, _callback);
            });
        };

        _bindButtonEvent();
    };

})(jQuery);