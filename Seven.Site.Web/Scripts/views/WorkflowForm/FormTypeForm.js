﻿/*
==============================================================================
//  表单类型新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.WorkflowForm.FormTypeForm");

    window.views.WorkflowForm.FormTypeForm.initPage = function (form) {

        var _form = $("#" + form);

        var _key = $("#ID", _form).val();

        var _controlInit = function () {
            $("#Name", _form).validatebox({
                required: true
            });

            $("#SortNumber", _form).numberbox({
                required: true
            });

            $("#Remark", _form).attr(window.comlib.textAreaAttr);
        };

        var _bindButtonEvent = function () {
            $("#select_company", _form).click(function () {
                var el = $("#CompanyID", _form);
                window.comlib.showCompanySelector(function (node) {
                    if (node) {
                        el.val(node.id);
                        el.next().text(node.text);
                    }
                }, el.val(), true);
            });
        };

        _controlInit();
        _bindButtonEvent();
    };

})(jQuery);