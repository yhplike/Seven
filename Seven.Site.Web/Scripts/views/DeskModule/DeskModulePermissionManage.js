﻿(function ($) {

    $.util.namespace("views.DeskModule.DeskModulePermissionManage");

    var tree = "#tree", treePanel = "#treePanel", dg = "#dg", treeOther = "#tree-other";

    window.views.DeskModule.DeskModulePermissionManage.initPage = function (permissions) {
        var data = window.jeasyui.helper.buildToolbarUsePermissionJson(permissions, true);
        data.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.DeskModule.reloadDeskModulePermission(); } } });

        var treeInit = function () {
            $(tree).tree({
                url: "/Public/OrganizationUserJson",
                animate: true,
                onSelect: function (node) {
                    if (node.attributes.IsUser) {
                        $(dg).datagrid("getPanel").panel("setTitle", "[" + node.text + "] - 桌面模块权限设置");
                        window.views.DeskModule.reloadDeskModulePermission(node.attributes.UserID);
                    }
                },
                onBeforeSelect: function (node) {
                    return node.attributes.IsUser == true;
                },
                onBeforeLoad: function (node, param) {
                    $.easyui.loading({ locale: $(this).currentRegion(), msg: "数据加载中..." });
                },
                onLoadSuccess: function (node) {
                    $.easyui.loaded($(this).currentRegion());
                    if (node == null) {
                        $(this).tree("expandAll");
                    }
                }
            });
        };

        var dataGridInit = function () {
            var toolbar = $("<div />").appendTo("body").toolbar({
                data: data
            }).toolbar("toolbar");

            var frozenColumns = function (showCheckBox) {
                var result = [];
                if (showCheckBox) {
                    result.push({ field: 'ck', checkbox: true });
                }
                var normal = [{
                    field: 'ID', title: 'ID', width: 100
                }, {
                    field: 'Name', title: '桌面模块', width: 200
                }];
                $.util.merge(result, normal);
                return result;
            };

            var options = {
                idField: "ID",
                title: "桌面模块权限",
                toolbar: toolbar,
                striped: true,
                fit: true,
                border: false,
                singleSelect: false,
                frozenColumns: [frozenColumns(true)],
                onLoadSuccess: function (data) {
                    $.fn.datagrid.extensions.onLoadSuccess.apply(this, arguments);//调用扩展的事件防止覆盖
                    if (data.rows.length > 0) {
                        var t = $(this);
                        $.each(data.rows, function (i, row) {
                            if (row.Permission == true) {
                                t.datagrid("checkRow", t.datagrid("getRowIndex", row.ID));
                            }
                        });
                    }
                }
            };

            $(dg).datagrid(options);
        };

        treeInit();
        dataGridInit();
    };

    window.views.DeskModule.reloadDeskModulePermission = function (userId) {
        if (!userId) { var selectUser = $(tree).tree("getSelected"); if (selectUser && selectUser.attributes.IsUser) { userId = selectUser.attributes.UserID; } }
        if (userId) {
            $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
            $.post("/DeskModule/UserDeskModulePermissionJson", { userId: userId }, function (data) {
                $.easyui.loaded($("body"));
                $(dg).datagrid("clearSelections");
                $(dg).datagrid("clearChecked");
                $(dg).datagrid("loadData", data);
            });
        }
    };

    window.views.DeskModule.savePermission = function () {
        var userSelected = $(tree).tree("getSelected");
        if (userSelected && userSelected.attributes.IsUser) {
            var rows = $(dg).datagrid("getChecked");
            var arrayPermission = $.array.map(rows, function (row) { return row.ID; });
            var param = { deskModuleIds: arrayPermission.join(","), userId: userSelected.attributes.UserID };
            $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
            $.post("/DeskModule/SaveUserDeskModulePermission", param, function (result) {
                $.easyui.loaded($("body"));
                window.jeasyui.helper.actionDoneCallBack.call(this, result);
            });
        }
    };

})(jQuery);