﻿/*
==============================================================================
//  用户权限管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.User.PermissionManage");

    var tree = "#tree1", tg = "#tg"; var lastNodeId;

    window.views.User.PermissionManage.initPage = function (permissions) {
        var data = window.jeasyui.helper.buildToolbarUsePermissionJson(permissions, true);
        data.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.User.reloadPermissionAndOtherUser(); } } });

        var initTreeReloadButton = function () {
            var toolbar = $("<div />").insertBefore(tree).toolbar({
                data: [{
                    type: "button", options: {
                        text: "刷新", iconCls: "icon-reload", onclick: function (t) {
                            var oldNode = $(tree).tree("getSelected");
                            if (oldNode) { lastNodeId = oldNode.id; }
                            $(tree).tree("reload");
                        }
                    }
                }]
            }).toolbar("toolbar");
        };

        var treeInit = function () {

            var options = {
                url: "/Public/OrganizationUserJson",
                animate: true,
                toggleOnClick: true,
                onSelect: function (node) {
                    if (node.attributes.IsUser) {
                        $(tg).treegrid("getPanel").panel("setTitle", "[" + node.text + "] - 用户权限设置");
                        window.views.User.reloadPermissionAndOtherUser(node.attributes.UserID);
                    }
                },
                onBeforeLoad: function (node, param) {
                    $.easyui.loading({ locale: $(this).currentRegion(), msg: "数据加载中..." });
                },
                onLoadSuccess: function (node) {
                    $.easyui.loaded($(this).currentRegion());
                    if (node == null) {
                        $(this).tree("expandAll");
                    }
                }
            };

            $(tree).tree(options);
        };

        var treeGridInit = function () {
            var toolbar = $("<div />").appendTo("body").toolbar({
                data: data
            }).toolbar("toolbar");

            var frozenColumns = function (showCheckBox) {
                var result = [];
                if (showCheckBox) {
                    result.push({ field: 'ck', checkbox: true });
                }
                var normal = [{ field: 'Name', title: '菜单', width: 200 }];
                $.util.merge(result, normal);
                return result;
            };

            var columns = function (sortableEnable) {
                var result = [];
                var normal = [
                        {
                            field: 'Permission', title: '权限', width: 1350, align: "left",
                            formatter: function (val, row) {
                                var html = "";
                                if (row.Permissions && row.Permissions.length && row.Permissions.length > 0) {
                                    $.each(row.Permissions, function (i, p) {
                                        html += "<span id=\"checkspan_" + p.MenuPermissionID + "_end\" name=\"checkspan_" + row.ID + "_end\" class=\"tree-checkbox tree-checkbox" + p.State + "\" key=\"" + p.MenuPermissionID + "\" value=\"" + row.ID + "\" onclick=\"window.views.User.checkState(this, event);\" ></span><b>" + p.MenuPermissionName + "</b>&nbsp;&nbsp;&nbsp;&nbsp;";
                                    });
                                }
                                return html;
                            }
                        },
                ];
                if (sortableEnable) {
                    $.array.forEach(normal, function (val, index, array) {
                        val.sortable = true;
                    });
                }
                $.util.merge(result, normal);
                return result;
            };

            var options = {
                title: "用户权限设置",
                idField: "ID",
                treeField: "Name",
                dataPlain: true,
                parentField: "ParentID",
                toolbar: toolbar,
                rownumbers: true,
                fit: true,
                border: false,
                singleSelect: true,
                checkOnSelect: false,
                selectOnCheck: false,
                cascadeCheck: true,
                enableHeaderClickMenu: false,
                frozenColumns: [frozenColumns(true)],
                columns: [columns(false)],
                onCheck: function (rowData) {
                    var dom = $(this).treegrid("getRowDom", rowData.ID);
                    $("span[name='checkspan_" + rowData.ID + "_end']", dom).removeClass("tree-checkbox0 tree-checkbox2").addClass("tree-checkbox1");
                },
                onUncheck: function (rowData) {
                    var dom = $(this).treegrid("getRowDom", rowData.ID);
                    $("span[name='checkspan_" + rowData.ID + "_end']", dom).removeClass("tree-checkbox1 tree-checkbox2").addClass("tree-checkbox0");
                },
                onCheckAll: function (rows) {
                    var dgPanel = $(this).treegrid("getPanel");
                    $("span[id^='checkspan_']", dgPanel).removeClass("tree-checkbox0 tree-checkbox2").addClass("tree-checkbox1");
                },
                onUncheckAll: function (rows) {
                    var dgPanel = $(this).treegrid("getPanel");
                    $("span[id^='checkspan_']", dgPanel).removeClass("tree-checkbox1 tree-checkbox2").addClass("tree-checkbox0");
                }
            };

            $(tg).treegrid(options);
        };

        initTreeReloadButton();
        treeInit();
        treeGridInit();
    };

    window.views.User.checkState = function (ck, e) {
        ck = $(ck);
        if (ck.hasClass("tree-checkbox0")) {
            ck.removeClass("tree-checkbox0").addClass("tree-checkbox2");
        }
        else if (ck.hasClass("tree-checkbox1")) {
            ck.removeClass("tree-checkbox1").addClass("tree-checkbox0");
        }
        else {
            ck.removeClass("tree-checkbox2").addClass("tree-checkbox1");
        }
        e.stopPropagation();
    };

    window.views.User.reloadPermissionAndOtherUser = function (userId, reloadTree) {
        if (!userId) { var selectUser = $(tree).tree("getSelected"); if (selectUser && selectUser.attributes.IsUser) { userId = selectUser.attributes.UserID; } }
        reloadTree = reloadTree ? reloadTree : false;
        if (userId) {
            $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
            $.post("/User/UserMenuPermissionJson", { userId: userId }, function (data) {
                $.easyui.loaded($("body"));
                $(tg).treegrid("clearSelections");
                $(tg).treegrid("clearChecked");
                $(tg).treegrid("loadData", data);
            });
            //if (reloadTree) { $(userTreeOther).tree("reload"); }
        }
    };

    window.views.User.saveUserPermission = function (showOther) {
        var selectUser = $(tree).tree("getSelected");
        if (selectUser && selectUser.attributes.IsUser) {
            var dgPanel = $(tg).treegrid("getPanel");
            var currentRowCheckbox = $("span[id^='checkspan_']", dgPanel);
            var arrayPermission = [];
            currentRowCheckbox.each(function (i, el) {
                var check = !$(el).hasClass("tree-checkbox0");
                var indeterminate = $(el).hasClass("tree-checkbox2");
                arrayPermission.push({ MenuID: $(el).attr("value"), MenuPermissionID: $(el).attr("key"), Check: check, Indeterminate: indeterminate });
            });

            var param = { permission: JSON.stringify(arrayPermission), userId: selectUser.attributes.UserID };
            if (!showOther) {
                //var otherselectUser = $(userTreeOther).tree("getChecked");
                //var otherUserId = [];
                //$.array.forEach(otherselectUser, function (val) {
                //    if (val.attributes.IsUser) {
                //        otherUserId.push(val.attributes.ID);
                //    }
                //});

                //$.extend(param, { otherUserIds: otherUserId.join(",") });
            }
            $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
            $.post("/User/SaveUserPermission", param, function (result) {
                $.easyui.loaded($("body"));
                window.jeasyui.helper.actionDoneCallBack.call(this, result);
            });
        }
    };

})(jQuery);