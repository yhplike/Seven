﻿
(function () {

    var pluginName = "seven_preview", commandName = "sevenPreview", toolButtonName = "sevenPreview", iconPathPostfix = "images/preview.png", htmlUrlPostfix = "../Scripts/jquery-plugins/ckeditor/plugins/seven_preview/preview.html";

    CKEDITOR.plugins.add(pluginName, {
        init: function (editor) {
            editor.addCommand(commandName, {
                canUndo: false,
                modes: { wysiwyg: 1, source: 1 }, //在可视化、源码状态下都允许执行命令
                exec: function (editor) {
                    var content = editor.getData(),
                        config = editor.config,
                        baseTag = config.baseHref ? "<base href=\"" + config.baseHref + "\" />" : "";
                    isCustomDomain = CKEDITOR.env.isCustomDomain();

                    if (config.fullPage) {
                        content = content.replace(/<head>/, '$&' + baseTag).replace(/[^>]*(?=<\/title>)/, '$& &mdash; ' + "预览");
                    }
                    else {
                        var bodyHtml = "<body ",
                            body = editor.document && editor.document.getBody();

                        if (body) {
                            var idAttr = body.getAttribute("id");
                            if (idAttr) {
                                bodyHtml += "id=\"" + idAttr + "\"";
                            }
                            var classAttr = body.getAttribute("class");
                            if (classAttr) {
                                bodyHtml += "class=\"" + classAttr + "\"";
                            }
                        }

                        bodyHtml += ">";
                    }

                    var size = $.util.windowSize(), iWidth = size.width * 0.8, iHeight = size.height * 0.7, iLeft = (size.width - iWidth) / 2;

                    var windowUrl = htmlUrlPostfix;
                    window._cke_htmlToLoad = content;

                    if (isCustomDomain) {
                        windowUrl = 'javascript:void( (function(){' +
                        'document.open();' +
                        'document.domain="' + document.domain + '";' +
                        'document.write( window.opener._cke_htmlToLoad );' +
                        'document.close();' +
                        'window.opener._cke_htmlToLoad = null;' +
                        '})() )';
                    }

                    window.open(windowUrl, null, "toolbar=yes,location=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=" + iWidth + ",height=" + iHeight + ",left=" + iLeft);
                }
            });
            var iconPath = this.path + iconPathPostfix;
            editor.ui.addButton(toolButtonName, {
                label: "预览",
                command: commandName,
                icon: iconPath
            });
        }
    });

})();
