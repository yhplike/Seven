﻿(function () {

    var dialogName = "seven_calculate";

    CKEDITOR.dialog.add(dialogName, function (editor) {
        //常规属性的赋值，如value、size、maxLength、title
        var autoSetup1 = function (element) {
            var value = element.getAttribute(this.id) || "";
            this.setValue(value);
        };

        //单位是px的样式属性的赋值
        var autoSetup2 = function (element) {
            var value = CKEDITOR.tools.convertToPx(element.getStyle(this.id)) || "";
            this.setValue(value);
        };

        //单位不是px的样式属性的赋值
        var autoSetup3 = function (element) {
            var value = element.getStyle(this.id) || "";
            this.setValue(value);
        };

        //常规属性的提交，如value、size、maxLength、title
        var autoCommit1 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setAttribute(this.id, value); }
            else
            { element.removeAttribute(this.id); }
        };

        //单位是px的样式属性的提交
        var autoCommit2 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setStyle(this.id, CKEDITOR.tools.cssLength(value)); }
            else
            { element.removeStyle(this.id); }
        };

        //单位不是px的样式属性的提交
        var autoCommit3 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setStyle(this.id, value); }
            else
            { element.removeStyle(this.id); }
        };

        var htmlAttributes = ["calculateFormula", "title"];
        var pxCssAttributes = ["width", "font-size"];
        var nopxCssAttributes = ["text-align"];

        var onShow = function () {
            delete this.calculate;

            var element = editor.getSelection().getSelectedElement();
            if (element && element.getName() == "input") {
                var elementType = element.getAttribute("type");
                if (elementType == "text") {
                    var des = this.getContentElement("info", "calculateDes").getElement();
                    des.hide();

                    this.calculate = element;
                    this.setupContent(element);
                }
            }
        };

        var onOk = function () {
            var element = this.calculate, isInsertMode = !element;
            if (isInsertMode) {
                element = editor.document.createElement("input");
                element.setAttribute("type", "text");
                element.addClass("Calculate");
                element.addClass("CalculateSelf");
                editor.insertElement(element);
            }
            this.commitContent({ element: element });//此处的参数传入方式，决定了element的commit属性值的参数签名
        };

        var onLoad = function () {
            this.foreach(function (contentObj) {
                if (htmlAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup1;
                    contentObj.commit = autoCommit1;
                }
                else if (pxCssAttributes.contains(contentObj.id)) {
                    contentObj.validate = CKEDITOR.dialog.validate.integer("请输入数字格式。");
                    contentObj.setup = autoSetup2;
                    contentObj.commit = autoCommit2;
                }
                else if (nopxCssAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup3;
                    contentObj.commit = autoCommit3;
                }
            });
        };

        var controlOptions = {
            id: "info", label: "计算控件属性", title: "计算控件属性",
            elements: [
                { id: "title", type: "text", label: "控件名称", "default": "" },
                {
                    type: "hbox", widths: ["50%", "50%"],
                    children: [
                        { id: "width", type: "text", label: "控件宽度", "default": "" },
                        { id: "font-size", type: "text", label: "字体大小", "default": "" }
                    ]
                },
                { id: "calculateFormula", type: "textarea", label: "计算公式", rows: 7, cols: 4, "default": "" },
                {
                    id: "readonly", type: "checkbox", label: "允许编辑", "default": "",
                    setup: function (element) {
                        var originalChecked = element.getAttribute("readonly");
                        this.setValue(originalChecked == "readonly" ? "" : "checked");
                    },
                    commit: function (data) {
                        var element = data.element,
                            originalChecked = element.getAttribute("readonly"),
                            currentChecked = this.getValue();
                        if ((originalChecked == null && !currentChecked)) {
                            element.setAttribute("readonly", "readonly");
                        }
                        else if ((originalChecked == "readonly") == currentChecked) {
                            var newElement = CKEDITOR.dom.element.createFromHtml("<input type=\"text\" " + (!currentChecked ? "readonly=\"readonly\"" : "") + " />");
                            element.copyAttributes(newElement, { readonly: 1 }); //value表示copy过程中要忽略的属性
                            newElement.replace(element); //表示用newElement替换element，源和目标刚好与js的replace刚好相反
                            editor.getSelection().selectElement(newElement);
                            data.element = newElement;
                        }
                    }
                },
                {
                    type: "vbox", widths: ["50%", "50%"],
                    children: [
                        {
                            type: "html", html: "<span style=\"color:blue;cursor: pointer\">计算公式填写说明</span>",
                            onClick: function () {
                                var dialog = this.getDialog();
                                var des = dialog.getContentElement("info", "calculateDes").getElement();
                                if (des.isVisible())
                                { des.hide(); }
                                else
                                { des.show(); }
                            }
                        },
                        {
                            id: "calculateDes", type: "html",
                            html: '<div style="font-size:10px;color="blue"">计算公式支持[+ - * /]和英文括号以及特定计算函数<br/>' +
                                            '例如：(数值1+数值2)*数值3-ABS(数值4)<br/>' +
                                            '其中数值1、数值2是表单控件名称。<br/>' +
                                            '当前版本所支持的计算函数：<br/>' +
                                            '1、MAX(数值1,数值2,数值3...) 输出最大值,英文逗号分隔;<br/>' +
                                            '2、MIN(数值1,数值2,数值3...) 输出最小值,英文逗号分隔;<br/>' +
                                            '3、ABS(数值1) 输出绝对值;<br/>' +
                                            '4、MOD(数值1,数值2) 计算数值1除数值2的余数;英文逗号分隔;<br/>' +
                                            '5、AVG(数值1,数值2,数值3...) 输出平均值;英文逗号分隔;<br/>' +
                                            '6、SUM(数值1,数值2,数值3...) 输出求和;英文逗号分隔;</br>' +
                                            '7、DATE(开始时间,结束时间) 输出整天或半天;</br>' +
                                            '8、DAY(开始时间,结束时间) 输出整天;</div>'
                        }
                    ]
                }
            ]
        };

        var options = {
            title: "计算控件属性",
            minWidth: 400,
            minHeight: 210,
            height: 220,
            width: 400,
            onShow: onShow,
            onOk: onOk,
            onLoad: onLoad,
            contents: [controlOptions]
        };

        return options;
    });

})();