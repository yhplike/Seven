﻿(function () {

    var dialogName = "seven_macro";

    CKEDITOR.dialog.add(dialogName, function (editor) {
        //常规属性的赋值，如value、size、maxLength、title
        var autoSetup1 = function (element) {
            var value = element.getAttribute(this.id) || "";
            this.setValue(value);
        };

        //单位是px的样式属性的赋值
        var autoSetup2 = function (element) {
            var value = CKEDITOR.tools.convertToPx(element.getStyle(this.id)) || "";
            this.setValue(value);
        };

        //常规属性的提交，如value、size、maxLength、title
        var autoCommit1 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setAttribute(this.id, value); }
            else
            { element.removeAttribute(this.id); }
        };

        //单位是px的样式属性的提交
        var autoCommit2 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setStyle(this.id, CKEDITOR.tools.cssLength(value)); }
            else
            { element.removeStyle(this.id); }
        };

        var htmlAttributes = ["value", "title", "macroType"];
        var pxCssAttributes = ["width", "font-size"];

        var onShow = function () {
            delete this.macro;

            var element = editor.getSelection().getSelectedElement();
            if (element) {
                this.macro = element;
                this.setupContent(element);
            }
        };

        var onOk = function () {
            var element = this.macro, isInsertMode = !element;
            if (isInsertMode) {
                element = editor.document.createElement("input");
                element.setAttribute("type", "text");
                element.addClass("Macro");
                element.setAttribute("readonly", true);
                element.setAttribute("value", "{MACRO}");
                editor.insertElement(element);
            }
            this.commitContent({ element: element });//此处的参数传入方式，决定了element的commit属性值的参数签名
        };

        var onLoad = function () {
            this.foreach(function (contentObj) {
                if (htmlAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup1;
                    contentObj.commit = autoCommit1;
                }
                else if (pxCssAttributes.contains(contentObj.id)) {
                    contentObj.validate = CKEDITOR.dialog.validate.integer("请输入数字格式。");
                    contentObj.setup = autoSetup2;
                    contentObj.commit = autoCommit2;
                }
            });
        };

        var controlOptions = {
            id: "macro",
            label: "宏控件",
            title: "宏控件",
            elements: [
                { id: "title", type: "text", label: "宏控件名称", style: "width:150px;", "default": "" },
                {
                    type: "hbox", widths: ["50%", "50%"],
                    children: [
                        { id: "width", type: "text", label: "宏控件宽度", "default": "" },
                        { id: "font-size", type: "text", label: "字体大小", "default": "" },
                    ]
                },
                {
                    id: "macroType", type: "select", label: "宏控件类型", "default": "",
                    items: [["<请选择>", ""], ["当前日期", "SYS_DATE"], ["当前用户姓名", "SYS_REALNAME"], ["当前用户所属部门（全称）", "SYS_DEPTNAME_FULL"], ["当前用户所属部门（简称）", "SYS_DEPTNAME_SHORT"], ["当前用户所属岗位", "SYS_POSITIONNAME"]]
                }
            ]
        };

        var options = {
            title: "宏控件属性",
            minWidth: 400,
            minHeight: 90,
            height: 150,
            width: 400,
            onShow: onShow,
            onOk: onOk,
            onLoad: onLoad,
            contents: [controlOptions]
        };

        return options;
    });

})();