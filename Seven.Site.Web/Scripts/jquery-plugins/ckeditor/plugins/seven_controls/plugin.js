﻿(function () {

    var pluginName = "seven_controls", commandName = "sevenControls", toolButtonName = "sevenControls", iconPathPostfix = "images/help.png";

    CKEDITOR.plugins.add(pluginName, {
        init: function (editor) {
            var lang = editor.lang;

            var addControlCommand = function (commandName, dialogName, dialogFilePath) {
                editor.addCommand(commandName, new CKEDITOR.dialogCommand(dialogName));
                CKEDITOR.dialog.add(dialogName, dialogFilePath);
            };

            var dialogPath = this.path + 'dialogs/';

            addControlCommand("sevenTextfield", "seven_textfield", dialogPath + "seven_textfield.js");
            addControlCommand("sevenTextarea", "seven_textarea", dialogPath + "seven_textarea.js");
            addControlCommand("sevenCheckbox", "seven_checkbox", dialogPath + "seven_checkbox.js");
            addControlCommand("sevenDatetimebox", "seven_datetimebox", dialogPath + "seven_datetimebox.js");
            addControlCommand("sevenMacro", "seven_macro", dialogPath + "seven_macro.js");
            addControlCommand("sevenCalculate", "seven_calculate", dialogPath + "seven_calculate.js");

            if (editor.contextMenu && editor.addMenuItems) {
                editor.addMenuGroup("seven_textfield", 10);
                editor.addMenuGroup("seven_textarea", 11);
                editor.addMenuGroup("seven_checkbox", 12);
                editor.addMenuGroup("seven_datetimebox", 13);
                editor.addMenuGroup("seven_macro", 14);
                editor.addMenuGroup("seven_calculate", 15);

                editor.addMenuItem("seven_textfield", { label: "单行文本框属性", command: "sevenTextfield", group: "seven_textfield" });
                editor.addMenuItem("seven_textarea", { label: "多行文本框属性", command: "sevenTextarea", group: "seven_textarea" });
                editor.addMenuItem("seven_checkbox", { label: "复选框属性", command: "sevenCheckbox", group: "seven_checkbox" });
                editor.addMenuItem("seven_datetimebox", { label: "日期控件属性", command: "sevenDatetimebox", group: "seven_datetimebox" });
                editor.addMenuItem("seven_macro", { label: "宏控件属性", command: "sevenMacro", group: "seven_macro" });
                editor.addMenuItem("seven_calculate", { label: "计算控件属性", command: "sevenCalculate", group: "seven_calculate" });

                editor.contextMenu.addListener(function (element) {
                    if (element && !element.isReadOnly()) {
                        var name = element.getName();
                        if (name == "input") {
                            switch (element.getAttribute("type")) {
                                case "text":
                                    if (element.hasClass("Wdate"))
                                    { return { seven_datetimebox: CKEDITOR.TRISTATE_OFF }; }
                                    else if (element.hasClass("Macro"))
                                    { return { seven_macro: CKEDITOR.TRISTATE_OFF }; }
                                    else if (element.hasClass("Calculate"))
                                    { return { seven_calculate: CKEDITOR.TRISTATE_OFF }; }

                                    return { seven_textfield: CKEDITOR.TRISTATE_OFF };
                                case "checkbox":
                                    return { seven_checkbox: CKEDITOR.TRISTATE_OFF };
                            }
                        }
                        else if (name == "textarea")
                        { return { seven_textarea: CKEDITOR.TRISTATE_OFF }; }
                    }
                });
            }

            editor.on("doubleclick", function (evt) {
                var element = evt.data.element;
                if (element.is("input")) {
                    switch (element.getAttribute("type")) {
                        case "text":
                            if (element.hasClass("Wdate"))
                            { evt.data.dialog = "seven_datetimebox"; }
                            else if (element.hasClass("Macro"))
                            { evt.data.dialog = "seven_macro"; }
                            else if (element.hasClass("Calculate"))
                            { evt.data.dialog = "seven_calculate"; }
                            else { evt.data.dialog = "seven_textfield"; }
                            break;
                        case "checkbox":
                            evt.data.dialog = "seven_checkbox";
                            break;
                    }
                }
                else if (element.is("textarea")) {
                    evt.data.dialog = 'seven_textarea';
                }
            });
        }
    });
})();