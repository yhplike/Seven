﻿
(function () {

    var pluginName = "seven_help", commandName = "sevenHelp", toolButtonName = "sevenHelp", iconPathPostfix = "images/help.png";

    CKEDITOR.plugins.add(pluginName, {
        init: function (editor) {
            editor.addCommand(commandName, {
                modes: { wysiwyg: 1, source: 1 }, //在可视化、源码状态下都允许执行命令
                exec: function (editor) {
                    alert("测试帮助信息");
                }
            });
            var iconPath = this.path + iconPathPostfix;
            editor.ui.addButton(toolButtonName, {
                label: "帮助",
                command: commandName,
                icon: iconPath
            });
        }


    });

})();