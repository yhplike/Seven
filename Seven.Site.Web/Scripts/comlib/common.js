﻿(function ($) {

    $.util.namespace("comlib");

    window.comlib.loginPageUrl = "/Account/Login";

    //整个项目的textare的所占行数和列数
    window.comlib.textAreaAttr = { cols: "3", rows: "3" };
    window.comlib.textAreaReadonlyAttr = { cols: "3", rows: "3", readonly: "readonly" };
    window.comlib.textAreaAttrBig = { cols: "4", rows: "5" };
    window.comlib.textAreaAttrBigger = { cols: "4", rows: "7" };

    //整个项目的表示颜色
    window.comlib.color = { green: "#66CD00;", red: "red;", purple: "#6600FF", sky: "#3366FF", pink: "#FF99FF" };

    //整个项目的状态表示颜色
    window.comlib.stateColor = { green: "color:#66CD00;", red: "color:red;", purple: "color:#6600FF", sky: "color:#3366FF" };

    //获取图标
    window.comlib.icon = function (iconCls) {
        if ($.string.isNullOrWhiteSpace(iconCls)) { return ""; }
        return "<span class=\"list-span " + iconCls + "\"></span>";
    }

    //以linkbutton的方式给datagrid的操作列格式化出按钮
    // htmlAttributes：object类型，为a标签定义html属性，如id，name，class等
    // inconCls：string类型，icon图标的css名
    // text：string类型，按钮的中文信息
    window.comlib.formatLinkButton = function (htmlAttributes, iconCls, text) {
        var a = $("<a />");
        if (typeof htmlAttributes == "object") {
            a.attr(htmlAttributes);
        }
        a.addClass("l-btn l-btn-plain");
        var spanOutside = $("<span />"), spanInside = $("<span />");
        spanOutside.addClass("l-btn-left");
        spanInside.addClass("l-btn-text");
        if (typeof iconCls == "string") {
            spanInside.addClass(iconCls);
        }
        spanInside.addClass("l-btn-icon-left");
        if (typeof text == "string" && !$.string.isNullOrWhiteSpace(text)) {
            spanInside.text(text);
        }
        var lb = a.append(spanOutside.append(spanInside));
        return $("<div />").append(lb).html();
    };

    //window.open的简易封装
    // url:目标地址
    // param:传递参数，支持object和string格式
    // size:窗口大小，object格式，属性为width和height
    window.comlib.windowOpen = function (url, param, size) {
        if ($.string.isNullOrWhiteSpace(url)) { return; }
        if (typeof param == "object") {
            url = url + "?" + $.param(param);
        } else if (typeof param == "string") {
            url = url + "?" + param;
        }
        if (typeof size == "object") {
            if (size.width == undefined) { size.width = screen.width - 150 }
            if (size.height == undefined) { size.height = screen.height - 150 }
        } else { size = { width: screen.width - 150, height: screen.height - 150 }; }
        var sFeatures = "top=" + ((screen.height - size.height) / 2) + ",left=" + ((screen.width - size.width) / 2) + ",width=" + size.width + ",height=" + size.height + ",location=yes,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no";
        var tmp = window.open(url, "", sFeatures);
        //tmp.moveTo((screen.width - size.width) / 2, (screen.height - size.height) / 2);
        //tmp.resizeTo(size.width, size.height);
        //tmp.focus();
        //tmp.location = url;
    };

    ///////////////////////
    /// 给查询表单的text input控件添加回车查询事件
    ///////////////////////
    window.comlib.bindKeyDownToQueryFormInput = function (form, query) {
        form = form == undefined ? $("#queryFm") : form;
        query = query == undefined ? $("#aquery", form) : query;
        var el = $("input[type=text]", form);
        el.bind("keydown", function (event) {
            if (event.keyCode == 13) {
                query.click();
                event.preventDefault();
            }
        });
        if (el.length > 0) { el.first().focus().select(); }
    };




    //菜单选择器，单选
    //参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为node
    //       selected：已选项的id串，逗号相隔，或者数组
    //       removeId：要移除的node的id
    //       showRoot：是否显示“根目录”，默认为false
    window.comlib.showMenuSelector = function (onEnter, selected, removeId, showRoot) {
        showRoot = showRoot ? Boolean(showRoot) : false;
        var opts = {
            onEnter: onEnter,
            selected: selected,
            treeOptions: {
                url: "/Public/FullMenusJson",
                queryParams: { showRoot: showRoot },
                dataPlain: true,
                parentField: "ParentID",
                onLoadSuccess: function (node, data) {
                    if (removeId && data.length > 0) {
                        var tree = $(this);
                        if (String(removeId) != "0") {
                            var node = tree.tree("find", removeId);
                            if (node) { tree.tree("remove", node.target); }
                        }
                    }
                    //console.warn($.util.$(this).isEasyUI("tree"));
                }
            }
        };

        var dia = $.easyui.showTreeSelector(opts);
    };


    //公司选择器 - 单选
    //参数说明：
    //      onEnterClick，“确定”按钮的触发事件，签名为selections，已选项集合
    //      selected：已选项的id串，逗号相隔，或者数组
    //      filter：是否根据当前用户的管理范围进行过滤，默认为false
    window.comlib.showCompanySelector = function (onEnter, selected, filter) {
        filter = filter ? Boolean(filter) : false;
        var opts = {
            onEnter: onEnter,
            selected: selected,
            treeOptions: {
                url: "/Public/CompanyJson",
                queryParams: { filter: filter }
            }
        };

        var dia = $.easyui.showTreeSelector(opts);
    }



    //部门选择器 - 单选
    //该选择器是tree类型。数据提供方式为异步（由url和options控制）。
    //参数说明：
    //      onEnterClick，“确定”按钮的触发事件，签名为selections，已选项集合
    //      selected：已选项的id串，逗号相隔，或者数组
    //      filter：是否根据当前用户的管理范围进行过滤，默认为false
    window.comlib.showDeptSelector = function (onEnter, selected, filter) {
        filter = filter ? Boolean(filter) : false;
        var opts = {
            onEnter: onEnter,
            selected: selected,
            treeOptions: {
                url: "/Public/OrganizationJson",
                queryParams: { filter: filter }
            }
        };

        var dia = $.easyui.showTreeSelector(opts);
    }



    //员工选择器 - 单选
    //参数说明：
    //      onEnterClick，“确定”按钮的触发事件，签名为selections，已选项集合
    //      selected：已选项的id串，逗号相隔，或者数组
    //      companyId：公司id，只显示该公司的员工
    //      deptId：部门id，只显示该部门的员工
    window.comlib.showStaffSelector = function (onEnter, selected, companyId, deptId) {
        companyId = companyId != undefined ? Number(companyId) : 0;
        deptId = deptId != undefined ? Number(deptId) : 0;

        var organId = companyId == 0 ? deptId : companyId;

        var onSelectParamBuild = function (node) {
            var param = {};
            if (node.attributes.IsCompany) { param = { companyId: node.id }; }
            else if (node.attributes.IsDept) { param = { deptId: node.id }; }
            return param;
        };

        var options = {
            title: '员工选择',
            width: 850,
            height: 500,
            onEnter: onEnter,
            selected: selected,
            treeTitle: "组织结构列表",
            treeWidth: 180,
            treeOptions: {
                url: "/Public/OrganizationJsonWithSelfForTree?id=" + organId + "&flag=" + organId,
                dataPlain: true,
                parentField: "ParentID",
                onSelectParamBuild: onSelectParamBuild,
                onLoadSuccess: function (node, data) {
                    if (organId != 0 && node == null && data.length > 0) {
                        var t = $(this);
                        var temp = t.tree("find", organId);
                        if (temp) { t.tree("select", temp.target); }
                    }
                }
            },
            datagridOptions: {
                singleSelect: true,
                //toolbar: toolbar,
                url: "/Public/StaffJsonForSelectorGrid",
                queryParams: { first: (organId == 0 ? true : false), companyId: companyId, deptId: deptId, rangeFilter: true },
                selectedUrl: "/Public/StaffJsonForSelectedGrid",
                idField: 'ID',
                columns: [[
                    { field: 'ID', title: '工号', width: 120 },
                    { field: 'RealName', title: '姓名', width: 90 }
                ]],
                pagination: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            }
        };

        var dia = $.easyui.showTreeDblDataGridSelector(options);
    };



    //角色选择器 - 单选
    //参数说明：
    //      onEnterClick，“确定”按钮的触发事件，签名为selections，已选项集合
    //      selected：已选项的id串，逗号相隔，或者数组
    //      companyId：公司id，只显示该公司的角色
    window.comlib.showRoleSelector = function (onEnter, selected, companyId) {
        //生成查询按钮的id，为回车查询功能定位按钮
        var btnId = "btn" + $.util.guid("N");
        var toolbar = [
                { type: "label", options: { text: "名称：" } },
                {
                    type: "textbox", options: {
                        name: "name", width: 120,
                        keydown: function (event, t) {
                            if (event.keyCode == 13) {
                                $(t).find("#" + btnId).click();
                                event.preventDefault();
                            }
                        }
                    }
                },
                {
                    type: "button", options: {
                        text: "查询", iconCls: "icon-search", id: btnId,
                        onclick: function (t) {
                            window.jeasyui.helper.searchForSelector($(t), "SingleDataGrid", { companyId: companyId });
                        }
                    }
                }
        ];

        var opts = {
            title: "角色选择",
            onEnter: onEnter,
            extToolbar: true,
            selected: selected,
            datagridOptions: {
                singleSelect: true,
                toolbar: toolbar,
                url: "/Public/RoleJsonForSelectorGrid",
                queryParams: { companyId: companyId },
                idField: 'ID',
                columns: [[
                    { field: 'ID', title: '角色ID', width: 120 },
                    { field: 'Name', title: '角色名称', width: 120 }
                ]]
            }
        };

        //dia是选择器的dialog对象，其中datagrid对象被放在dia对象的datagrid属性中
        var dia = $.easyui.showSingleDataGridSelector(opts);
    }

})(jQuery);