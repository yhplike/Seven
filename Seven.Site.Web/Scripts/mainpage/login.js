﻿(function () {

    window.loginForm = "form";
    window.loginUserName = "#UserName";
    window.loginPassWord = "#Password";
    window.loginButton = ".login-on";
    window.errorMessage = ".error-msg";

    window.bindButtonEvent = function () {
        $(window.loginButton).click(function () {
            $(window.errorMessage).text("");
            var form = $(window.loginForm);
            var model = form.form("getData", "discard");

            if ($.string.isNullOrWhiteSpace(model.UserName)) {
                $(window.errorMessage).text("登录名不能为空！"); return false;
            }
            if ($.string.isNullOrWhiteSpace(model.Password)) {
                $(window.errorMessage).text("密码不能为空！"); return false;
            }
            $.messager.progress({ title: "操作提醒", text: "正在登录" });
            form.submit();
        });

        $(window.loginUserName).bind({
            "keydown": function (e) { if (e.keyCode == 13) { $(window.loginPassWord).focus(); } },
            "focus": function (e) { this.select(); }
        });
        $(window.loginPassWord).bind({
            "keydown": function (e) { if (e.keyCode == 13) { $(window.loginButton).focus(); } },
            "focus": function (e) { this.select(); }
        });
    };

})(jQuery);