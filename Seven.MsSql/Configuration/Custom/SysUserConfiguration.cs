﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.MsSql.Configuration
{
    partial class SysUserConfiguration
    {
        partial void SysUserConfigurationAppend()
        {
            //User 和 Staff 1对1关系
            HasRequired(m => m.Staff).WithMany().HasForeignKey(f => f.StaffID);
        }
    }
}
