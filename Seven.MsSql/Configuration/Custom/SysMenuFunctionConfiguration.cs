﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.MsSql.Configuration
{
    partial class SysMenuFunctionConfiguration
    {
        partial void SysMenuFunctionConfigurationAppend()
        {
            HasRequired(m => m.Menu).WithMany().HasForeignKey(f => f.MenuID);
        }
    }
}
