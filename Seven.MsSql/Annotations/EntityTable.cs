﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Metadata.Edm;

namespace Seven.MsSql.Annotations
{
    /// <summary>
    /// 表示 EntityFramework 环境中的 实体对象模型类型 与 数据库中对应表的信息。
    /// </summary>
    public class EntityTable
    {
        public EntitySet ModelSet { get; set; }

        public EntitySet StoreSet { get; set; }

        public EntityType ModelType { get; set; }

        public EntityType StoreType { get; set; }

        public Type EntityType { get; set; }

        public string TableName { get; set; }

        public string Schema { get; set; }
    }
}
