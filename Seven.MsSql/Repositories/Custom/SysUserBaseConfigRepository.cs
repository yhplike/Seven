﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.Tools.Extension;

namespace Seven.MsSql.Repositories
{
    public partial class SysUserBaseConfigRepository
    {
        /// <summary>
        /// 根据用户ID获取该用户的基本配置信息【无配置信息则返回新实例】
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public SysUserBaseConfig GetUserBaseConfig(int userId)
        {
            var data = this.Entities().Where(w => w.UserID == userId)
                .Select(s => new
                {
                    s.ID,
                    s.UserID,
                    s.NorthCollapse,
                    s.WestCollapse,
                    s.SouthCollapse,
                    s.DefaultThemeName,
                    s.DefaultRootMenuID,
                    s.CreateUserName,
                    s.CreateDate,
                    s.Timestamp
                })
                .FirstOrDefault();
            return data == null ? new SysUserBaseConfig { UserID = userId } : data.CastTo<SysUserBaseConfig>();
        }
    }
}
