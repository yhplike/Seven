﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.RoleAbout;
using Seven.Tools.Extension;

namespace Seven.MsSql.Repositories
{
    public partial class SysRoleMenuPermissionRepository
    {
        /// <summary>
        /// 获取指定角色的限定菜单权限列表数据
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <param name="menuIds">菜单ID集合</param>
        /// <param name="filterUncheck">是否过滤掉“未选中”数据</param>
        /// <returns></returns>
        public IEnumerable<RoleMenuPermissionGridModel> RoleMenuPermissionGrid(int roleId, IEnumerable<int> menuIds, bool filterUncheck)
        {
            var roleMenuPermission = this.Entities().Where(w => w.RoleID == roleId);
            if (filterUncheck) { roleMenuPermission = roleMenuPermission.Where(w => w.Check); }
            var menuPermission = new SysMenuPermissionRepository(this.dbContext).Entities();
            var menu = new SysMenuRepository(this.dbContext).Entities();

            var query = (from a in roleMenuPermission
                         join b in menuPermission on a.MenuPermissionID equals b.ID into ab
                         from menuPermissionInfo in ab.DefaultIfEmpty()
                         join c in menu on menuPermissionInfo.MenuID equals c.ID
                         select new
                         {
                             a.ID,
                             a.RoleID,
                             a.MenuPermissionID,
                             MenuPermissionName = menuPermissionInfo.Name,
                             MenuPermissionSortNumber = menuPermissionInfo.SortNumber,
                             MenuID = c.ID,
                             MenuSortNumber = c.SortNumber,
                             a.Check
                         });

            if (menuIds.Count() > 0) { query = query.Where(w => menuIds.Contains(w.MenuID)).OrderBy(o => o.MenuSortNumber).ThenBy(o => o.MenuPermissionSortNumber); }
            else { query = query.OrderBy(o => o.MenuSortNumber).ThenBy(o => o.MenuPermissionSortNumber); }

            return query.ToArray().Select(s => s.CastTo<RoleMenuPermissionGridModel>(false));
        }

        /// <summary>
        /// 根据角色ID获取该角色的菜单权限集合
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns></returns>
        public IEnumerable<SysRoleMenuPermission> GetRoleMenuPermissions(int roleId)
        {
            return this.Entities().Where(w => w.RoleID == roleId).ToArray();
        }

        /// <summary>
        /// 根据用户ID获取该用户的角色的菜单权限集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public IEnumerable<SysRoleMenuPermission> GetRoleMenuPermissionsByUserID(int userId)
        {
            var roleId = new SysUserRepository(this.dbContext).Entities().Where(w => w.ID == userId).Select(s => s.RoleID).FirstOrDefault();

            return GetRoleMenuPermissions(roleId);
        }

        /// <summary>
        /// 根据用户ID获取该用户对应的角色的有权使用的菜单权限集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetRangeMenuPermissions(int userId)
        {
            var roleId = new SysUserRepository(this.dbContext).Entities().Where(w => w.ID == userId).Select(s => s.RoleID).FirstOrDefault();

            var rolePermissionIDs = this.Entities().Where(w => w.RoleID == roleId && w.Check).Select(s => s.MenuPermissionID).ToArray();

            return new SysMenuPermissionRepository(this.dbContext).Entities().Where(w => rolePermissionIDs.Contains(w.ID)).ToArray();
        }
    }
}
