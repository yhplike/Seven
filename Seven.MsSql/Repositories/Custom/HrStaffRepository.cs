﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.DataModel.RepositoryModels.StaffAbout;
using Seven.DataModel.ViewModels.StaffAbout;
using Seven.Tools.Extension;
using Seven.Web.IntelligentQuery.Model;
using Seven.Web.IntelligentQuery.Extensions;

namespace Seven.MsSql.Repositories
{
    public partial class HrStaffRepository
    {
        /// <summary>
        /// 根据员工ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public StaffModel GetEditModelByID(int id)
        {
            var staff = this.Entities().Where(w => w.ID == id);
            var organ = new HrOrganizationRepository(this.dbContext).Entities();

            var query = (from a in staff
                         join b in organ on a.CompanyID equals b.ID into ab
                         from CompanyInfo in ab.DefaultIfEmpty()
                         join c in organ on a.DeptID equals c.ID into ac
                         from DeptInfo in ac.DefaultIfEmpty()
                         select new
                         {
                             a.ID,
                             a.RealName,
                             a.Sex,
                             a.CompanyID,
                             CompanyName = CompanyInfo == null ? "" : CompanyInfo.Name,
                             a.DeptID,
                             DeptName = DeptInfo == null ? "" : DeptInfo.Name,
                             a.Remark
                         }).FirstOrDefault();

            return query.CastTo<StaffModel>();
        }

        /// <summary>
        /// 获取员工列表数据，供分页使用
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="positionId">岗位ID</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <param name="total">数据总数，输出参数</param>
        /// <returns></returns>
        public IEnumerable<StaffGridModel> StaffGrid(int? companyId, int? deptId, int? positionId, string model, int page, int rows, out int total)
        {
            var staff = this.Entities();
            var user = new SysUserRepository(this.dbContext).Entities();
            var company = new HrOrganizationRepository(this.dbContext).Entities();

            var queryModel = this.ConvertQueryModel(model);
            if (queryModel != null) { staff = staff.Where(queryModel); }

            #region 查询条件组织

            if (companyId.HasValue)
            {
                staff = staff.Where(w => w.CompanyID == companyId.Value);
            }
            if (deptId.HasValue)
            {
                staff = staff.Where(w => w.DeptID == deptId.Value);
            }

            #endregion

            var query = (from a in staff
                         join b in user on a.ID equals b.StaffID into ab
                         from userInfo in ab.DefaultIfEmpty()
                         join c in company on a.CompanyID equals c.ID
                         select new
                         {
                             a.ID,
                             a.RealName,
                             a.Sex,
                             UserName = (userInfo != null) ? (userInfo.IsDeleted ? userInfo.UserName + " [已删除]" : userInfo.UserName) : "",
                             CompanyName = c.Name,
                             DeptName = a.Dept.Name
                         });
            total = query.Count();
            var result = query.OrderBy(o => o.ID).SplitPage(page - 1, rows).ToArray();

            return result.Select(s => s.CastTo<StaffGridModel>(false));
        }

        /// <summary>
        /// 获取未创建用户的员工列表数据，供分页使用
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="positionId">岗位ID</param>
        /// <param name="realName">员工名字</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <param name="total">数据总数，输出参数</param>
        /// <returns></returns>
        public IEnumerable<StaffGridWithoutUserRelationModel> StaffGridWithoutUserRelation(int? companyId, int? deptId, int? positionId, string realName, int page, int rows, out int total)
        {
            var staff = this.Entities().Where(w => !w.IsDeleted);
            var user = new SysUserRepository(this.dbContext).Entities();
            var company = new HrOrganizationRepository(this.dbContext).Entities();

            #region 查询条件组织

            if (companyId.HasValue)
            {
                staff = staff.Where(w => w.CompanyID == companyId.Value);
            }
            if (deptId.HasValue)
            {
                staff = staff.Where(w => w.DeptID == deptId.Value);
            }

            if (!string.IsNullOrWhiteSpace(realName))
            {
                staff = staff.Where(w => w.RealName.Contains(realName));
            }

            #endregion

            var query = (from a in staff
                         join b in user on a.ID equals b.StaffID into ab
                         from userInfo in ab.DefaultIfEmpty()
                         join c in company on a.CompanyID equals c.ID
                         select new
                         {
                             a.ID,
                             a.RealName,
                             a.Sex,
                             userInfo.UserName,
                             a.CompanyID,
                             CompanyName = c.Name,
                             DeptName = a.Dept.Name
                         }).Where(w => w.UserName == null);

            total = query.Count();
            var result = query.OrderBy(o => o.ID).SplitPage(page - 1, rows).ToArray();

            return result.Select(s => s.CastTo<StaffGridWithoutUserRelationModel>(false));
        }

        /// <summary>
        /// 根据员工主键获取员工列表数据
        /// </summary>
        /// <param name="ids">主键字符串，以英文逗号相连</param>
        /// <returns></returns>
        public IEnumerable<StaffGridModel> StaffGrid(string ids)
        {
            var staff = this.Entities();
            var user = new SysUserRepository(this.dbContext).Entities();
            var company = new HrOrganizationRepository(this.dbContext).Entities();

            #region 查询条件组织

            if (!string.IsNullOrWhiteSpace(ids))
            {
                IEnumerable<int> temp = ids.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s));
                staff = staff.Where(w => temp.Contains(w.ID));
            }

            #endregion

            var query = (from a in staff
                         join b in user on a.ID equals b.StaffID into ab
                         from userInfo in ab.DefaultIfEmpty()
                         join c in company on a.CompanyID equals c.ID
                         select new
                         {
                             a.ID,
                             a.RealName,
                             a.Sex,
                             userInfo.UserName,
                             CompanyName = c.Name,
                             DeptName = a.Dept.Name
                         });

            var result = query.ToArray();

            return result.Select(s => s.CastTo<StaffGridModel>(false));
        }
    }
}
