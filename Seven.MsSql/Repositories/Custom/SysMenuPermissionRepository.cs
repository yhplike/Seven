﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.ViewModels.MenuAbout;
using Seven.Tools.Extension;

namespace Seven.MsSql.Repositories
{
    public partial class SysMenuPermissionRepository
    {
        /// <summary>
        /// 根据菜单权限ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns></returns>
        public MenuPermissionModel GetEditModelByID(int id)
        {
            var data = this.Entities().Where(w => w.ID == id)
                .Select(s => new
                {
                    s.ID,
                    s.Name,
                    s.ButtonName,
                    s.IconCls,
                    s.HandlerName,
                    s.Type,
                    s.SortNumber,
                    s.GroupMark,
                    s.MenuID,
                    s.FunctionIDs,
                    s.IsShow,
                    s.Remark
                })
                .FirstOrDefault();

            return data.CastTo<MenuPermissionModel>();
        }

        /// <summary>
        /// 根据菜单ID获取菜单权限集合
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetMenuPermissionsByMenuID(int menuId)
        {
            return this.Entities().Where(w => w.IsShow && w.MenuID == menuId).OrderBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 根据菜单ID集合获取菜单权限集合
        /// </summary>
        /// <param name="menuIds">菜单ID集合</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetMenuPermissionsByMenuIDs(IEnumerable<int> menuIds)
        {
            return this.Entities().Where(w => w.IsShow && menuIds.Contains(w.MenuID)).OrderBy(o => o.MenuID).ThenBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 获取所有菜单权限集合
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetAllMenuPermissions()
        {
            return this.Entities().Where(w => w.IsShow).OrderBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 获取除指定id之外的类型为按钮的菜单权限集合
        /// </summary>
        /// <param name="ids">要排除的菜单权限ID集合</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetMenuButtonPermissionsExcept(IEnumerable<int> ids)
        {
            return this.Entities().Where(w => w.Type == EntityBasic.PermissionType.按钮 && w.IsShow && !ids.Contains(w.ID)).OrderBy(o => o.MenuID).ThenBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 根据菜单权限ID集合获取菜单权限集合
        /// </summary>
        /// <param name="ids">菜单权限ID集合</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetMenuPermissions(IEnumerable<int> ids)
        {
            return this.Entities().Where(w => ids.Contains(w.ID)).OrderBy(o => o.MenuID).ThenBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 获取除指定菜单权限ID集合之外的菜单权限集合
        /// </summary>
        /// <param name="ids">要排除的菜单权限ID集合</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetMenuPermissionsExcept(IEnumerable<int> ids)
        {
            return this.Entities().Where(w => !ids.Contains(w.ID)).OrderBy(o => o.MenuID).ThenBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 根据菜单权限ID集合获取菜单权限记录中的functionids集合
        /// </summary>
        /// <param name="ids">菜单权限ID集合</param>
        /// <returns></returns>
        public IEnumerable<string> GetMenuFunctionIDs(IEnumerable<int> ids)
        {
            return this.Entities().Where(w => ids.Contains(w.ID)).Select(s => s.FunctionIDs).ToArray();
        }
    }
}
