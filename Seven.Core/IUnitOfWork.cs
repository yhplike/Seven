﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;
using Seven.Core.IRepositories;

namespace Seven.Core
{
    /// <summary>
    /// 工作单元接口
    /// </summary>
    public partial interface IUnitOfWork
    {
        /// <summary>
        /// 获取仓储
        /// </summary>
        /// <typeparam name="TEntity">实体类型</typeparam>
        /// <typeparam name="TKey">TEntity实体的主键类型</typeparam>
        /// <typeparam name="R">仓储接口</typeparam>
        /// <returns>仓储实例</returns>
        R GetRepository<TEntity, TKey, R>()
            where TKey : struct
            where TEntity : EntityBase<TKey>
            where R : class, IBaseRepository<TEntity, TKey>;

        /// <summary>
        /// 开始数据事务
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// 提交工作单元
        /// </summary>
        /// <returns>受影响行数</returns>
        int Commit();

        /// <summary>
        /// 执行回滚事务
        /// </summary>
        void Rollback();
    }
}
