﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;

namespace Seven.Core.Helper
{
    /// <summary>
    /// 实体操作
    /// </summary>
    public static class EntityAction
    {
        /// <summary>
        /// 获取实体EntityAttribute特性中的TableName属性值，若无该特性，返回空值
        /// </summary>
        /// <typeparam name="TEntity">实体类型</typeparam>
        /// <typeparam name="TKey">实体主键类型</typeparam>
        /// <returns></returns>
        public static string GetTableName<TEntity, TKey>()
            where TKey : struct
            where TEntity : EntityBase<TKey>
        {
            string TableName = "";

            var Attr = typeof(TEntity).GetCustomAttributes(typeof(EntityAttribute), false);
            if (Attr.Length == 1)
            {
                TableName = (Attr[0] as EntityAttribute).TableName;
            }

            return TableName;
        }
    }
}
