﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.ViewModels.WorkflowAbout;

namespace Seven.Core.IRepositories
{
    public partial interface IWfFormTypeRepository
    {
        /// <summary>
        /// 根据表单类别ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">表单类别ID</param>
        /// <returns></returns>
        FormTypeModel GetEditModelByID(int id);

        /// <summary>
        /// 根据表单类别ID获取其类别名称
        /// </summary>
        /// <param name="id">表单类别ID</param>
        /// <returns></returns>
        string GetFormTypeName(int id);

        /// <summary>
        /// 根据父级ID获取下一级表单类型集合
        /// </summary>
        /// <param name="id">父级id</param>
        /// <returns></returns>
        IEnumerable<WfFormType> GetSons(int id);

        /// <summary>
        /// 根据表单类型ID集合，获取他们的下一级表单类型的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级表单类型数量
        /// </summary>
        /// <param name="ids">表单类型ID集合</param>
        /// <returns></returns>
        IDictionary<int, int> GetSonsCount(IEnumerable<int> ids);

        /// <summary>
        /// 根据表单类型ID判定其是否存在子集类型
        /// </summary>
        /// <param name="id">表单类型ID</param>
        /// <returns></returns>
        bool SonExist(int id);
    }
}
