﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.StaffAbout;
using Seven.DataModel.ViewModels.StaffAbout;

namespace Seven.Core.IRepositories
{
    public partial interface IHrStaffRepository
    {
        /// <summary>
        /// 根据员工ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        StaffModel GetEditModelByID(int id);

        /// <summary>
        /// 获取员工列表数据，供分页使用
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="positionId">岗位ID</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <param name="total">数据总数，输出参数</param>
        /// <returns></returns>
        IEnumerable<StaffGridModel> StaffGrid(int? companyId, int? deptId, int? positionId, string model, int page, int rows, out int total);

        /// <summary>
        /// 获取未创建用户的员工列表数据，供分页使用
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="positionId">岗位ID</param>
        /// <param name="realName">员工名字</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <param name="total">数据总数，输出参数</param>
        /// <returns></returns>
        IEnumerable<StaffGridWithoutUserRelationModel> StaffGridWithoutUserRelation(int? companyId, int? deptId, int? positionId, string realName, int page, int rows, out int total);

        /// <summary>
        /// 根据员工主键获取员工列表数据
        /// </summary>
        /// <param name="ids">主键字符串，以英文逗号相连</param>
        /// <returns></returns>
        IEnumerable<StaffGridModel> StaffGrid(string ids);
    }
}
