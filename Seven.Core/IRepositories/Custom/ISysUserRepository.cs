﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.EntityBasic;
using Seven.DataModel.RepositoryModels.UserAbout;
using Seven.DataModel.ViewModels.UserAbout;

namespace Seven.Core.IRepositories
{
    public partial interface ISysUserRepository
    {
        #region 获取简单模型数据

        IEnumerable<SimpleModel1> GetSimpleModelByDeptID(int deptId);

        #endregion

        /// <summary>
        /// 根据登录帐号获取用户实体
        /// </summary>
        /// <param name="account">登录帐号，可以是用户名或登录别名</param>
        /// <returns></returns>
        SysUser GetUserByAccount(string account);

        /// <summary>
        /// 根据用户ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns></returns>
        UserModel GetEditModelByID(int id);

        /// <summary>
        /// 检查账号（用户名、登录别名）是否存在
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="id">需要忽略的用户的主键值，默认值为0。新增时判定可不传key参数，更新时判定则key传当前用户的主键</param>
        /// <returns></returns>
        bool AccountExist(string account, int id);

        /// <summary>
        /// 判定指定角色ID下是否存在有效用户
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns></returns>
        bool UseableUserExistByRoleID(int roleId);

        /// <summary>
        /// 判定指定员工ID是否存在对应的用户
        /// </summary>
        /// <param name="staffId">员工ID</param>
        /// <returns></returns>
        bool UseableUserExistByStaffID(int staffId);

        /// <summary>
        /// 根据组织结构ID集合，获取他们的下属用户的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级组织结构数量
        /// </summary>
        /// <param name="ids">组织结构ID集合</param>
        /// <param name="type">组织结构类型，该参数决定搜索方式</param>
        /// <returns></returns>
        IDictionary<int, int> GetUsersCountByOrganIDs(IEnumerable<int> ids, OrganType type);

        /// <summary>
        /// 获取用户管理列表数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="positionId">岗位ID</param>
        /// <param name="roleId">角色ID</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        IEnumerable<UserGridModel> UserManageGrid(int? companyId, int? deptId, int? positionId, int? roleId, string model, int page, int rows);
    }
}
