﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Site.Log
{
    internal class LogerConfig
    {
        /// <summary>
        /// 日志文件的文件全名（含扩展名）
        /// </summary>
        public static string LogFileName = "TestLog.txt";

        /// <summary>
        /// 日志文件的文件相对路径
        /// </summary>
        public static string LogFilePath = "Log/";

        /// <summary>
        /// 日志文件的备份相对路径
        /// </summary>
        public static string LogFileBakPath = "Log/Bak/";
    }
}
