﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Site.Models.AccountModel
{
    public class MainPageModel
    {
        /// <summary>
        /// 系统名称
        /// </summary>
        public string SystemName { get; set; }

        /// <summary>
        /// 登录使用的帐号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 用户池中的身份令牌标识，启用远程服务池时该属性才有正确的值
        /// </summary>
        public Guid Token { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 上次使用的主菜单
        /// </summary>
        public int DefaultRootMenuID { get; set; }

        /// <summary>
        /// 上次使用的主题
        /// </summary>
        public string DefaultThemeName { get; set; }

        /// <summary>
        /// 首页北部panel是否折叠
        /// </summary>
        public bool North { get; set; }

        /// <summary>
        /// 首页西部panel是否折叠
        /// </summary>
        public bool West { get; set; }

        /// <summary>
        /// 首页南部panel是否折叠
        /// </summary>
        public bool South { get; set; }

        /// <summary>
        /// 是否使用远程服务池，决定是否调用 用户池、SignalR推送。
        /// </summary>
        public bool UserPool { get; set; }

        /// <summary>
        /// SignalRService站点的url
        /// </summary>
        public string SignalRServiceUrl { get; set; }

        /// <summary>
        /// SignalR的集线器名称
        /// </summary>
        public string SignalRHubName { get; set; }

        /// <summary>
        /// 本系统的用户在SignalR服务中的分组名
        /// </summary>
        public string SiteSignalRGroupName { get; set; }
    }
}
