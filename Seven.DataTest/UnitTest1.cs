﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using Seven.Entity;
using Seven.EntityBasic;
using Seven.DataModel;
using Seven.Core;
using Seven.MsSql.Context;
using Seven.Tools.Extension;
using Seven.Tools.Helper;
using Seven.Web.IntelligentQuery.Converter;
using Seven.Web.IntelligentQuery.Model;
using Seven.Web.IntelligentQuery.Extensions;

namespace Seven.DataTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int count = 100000;
            var oldList = new SysUser[count];

            for (int k = 0; k < count; k++)
            {
                oldList[k] = new SysUser
                {
                    ID = k,
                    UserName = "admin" + k,
                    LoginCode = "cat" + k,
                    IsDisabled = false,
                    PassWord = "000" + k,
                    StaffID = k,
                    Staff = new HrStaff
                    {
                        ID = k,
                        RealName = "老王" + k,
                        Sex = Sex.男,
                        CompanyID = k,
                        DeptID = k,
                        CreateDate = DateTime.Now,
                        CreateUserName = "admin" + k,
                        Remark = "员工备注" + k
                    },
                    CreateDate = DateTime.Now,
                    CreateUserName = "admin" + k,
                    Remark = "用户备注" + k
                };
            }

            var newList = new SysUser[count];
            Stopwatch stopwatch = new Stopwatch(); stopwatch.Start();
            for (int k = 0; k < count; k++)
            {
                newList[k] = oldList[k].Duplicate();
            }
            stopwatch.Stop();

            Console.WriteLine("花费：" + stopwatch.ElapsedMilliseconds + "毫秒");
        }

        [TestMethod]
        public void TestMethod2()
        {
            int count = 100000;
            var oldList = new SysUser[count];

            for (int k = 0; k < count; k++)
            {
                oldList[k] = new SysUser
                {
                    ID = k,
                    UserName = "admin" + k,
                    LoginCode = "cat" + k,
                    IsDisabled = false,
                    PassWord = "000" + k,
                    StaffID = k,
                    Staff = new HrStaff
                    {
                        ID = k,
                        RealName = "老王" + k,
                        Sex = Sex.男,
                        CompanyID = k,
                        DeptID = k,
                        CreateDate = DateTime.Now,
                        CreateUserName = "admin" + k,
                        Remark = "员工备注" + k
                    },
                    CreateDate = DateTime.Now,
                    CreateUserName = "admin" + k,
                    Remark = "用户备注" + k
                };
            }

            var newList = new SysUser[count]; Stopwatch stopwatch = new Stopwatch(); stopwatch.Start();
            for (int k = 0; k < count; k++)
            {
                newList[k] = oldList[k].CopyByDelegate<SysUser, int>();
            }
            stopwatch.Stop();

            Console.WriteLine("花费：" + stopwatch.ElapsedMilliseconds + "毫秒");
        }

        [TestMethod]
        public void TestForRedis()
        {
            var a = new Seven.NoSql.Redis.RedisRepository();
            a.Set("test1", 987);
            Console.WriteLine(a.Get<int>("test1"));
        }

        [TestMethod]
        public void TestForRedis1()
        {
            var a = new Seven.NoSql.Redis.RedisRepository();
            string listId = "matrixkey-list";
            a.AddItemToList(listId, "md51");
            a.AddItemToList(listId, "md51");
            a.AddItemToList(listId, "md53");
            a.AddItemToList(listId, "md54");
            a.AddItemToList(listId, "md52");

            a.AddRangeToList(listId, new List<string> { "md58","md59","md50" });

            var r1 = a.GetAllItemsFromList(listId).Count; //8个

            a.RemoveStartFromList(listId); //7个

            a.RemoveItemFromList(listId, "md59"); //6个

            Console.WriteLine(r1); //8

            Console.WriteLine(a.GetItemFromList(listId, 1)); //md53

            Console.WriteLine(a.GetListCount(listId)); //6


        }
    }
}
