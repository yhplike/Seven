﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seven.Pool.SignalR
{
    public class ConnectionModel
    {
        public ConnectionModel()
        {
            DateTime dt = DateTime.Now;
            this.FirstConnectTime = dt; 
            this.LastConnectTime = dt;
        }

        /// <summary>
        /// 该用户在用户池中的TokenGuid，为了对应用户池中的user。当允许多台设备同时登录时，该标识是唯一的对应关系。
        /// </summary>
        public Guid UserToken { get; set; }

        public string UserName { get; set; }

        public string GroupName { get; set; }

        public DateTime FirstConnectTime { get; set; }

        /// <summary>
        /// 最后连接时间，未实现相应功能，请忽略
        /// </summary>
        public DateTime LastConnectTime { get; set; }
    }
}