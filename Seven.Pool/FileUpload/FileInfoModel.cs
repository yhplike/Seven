﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seven.Pool.FileUpload
{
    public class FileInfoModel
    {
        /// <summary>
        /// 文件ID，由WebUploader插件生成
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 文件全名，带扩展名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 文件扩展名
        /// </summary>
        public string Ext { get; set; }

        /// <summary>
        /// 文件总大小
        /// </summary>
        public long Size { get; set; }

        /// <summary>
        /// 完整文件的MD5，由WebUploader插件生成
        /// </summary>
        public string MD5 { get; set; }

        /// <summary>
        /// 文件类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 文件上传时间，当传block的时候断开，可能file还未上传完整时，文件上传时间为null
        /// </summary>
        public DateTime? UploadDate { get; set; }

        /// <summary>
        /// 文件最后上传时间。当文件已存在，再次上传该文件时，不需要真的上传该文件，但会更新这个字段的时间
        /// </summary>
        public DateTime? LastUploadDate { get; set; }
    }
}