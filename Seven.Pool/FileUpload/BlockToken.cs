﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Seven.Tools;
using Seven.Tools.Collections.Concurrent;

namespace Seven.Pool.FileUpload
{
    public class BlockToken
    {
        private string _md5;
        private FileInfoModel _file;
        private BlockTokenCollection _tokensPool;
        private static BlockTokenCollection _tokens;

        #region 构造函数定义

        private BlockToken(Func<BlockTokenCollection> tokensPoolSelector)
        {
            Check.NotNull(tokensPoolSelector);
            this._tokensPool = tokensPoolSelector();
        }

        private BlockToken(string md5)
            : this(GetTokensPool)
        {
            this._md5 = md5 != null ? md5.Trim() : string.Empty;
            this.InitFile();
        }

        #endregion

        #region internal 静态属性定义 - 获取分片列表

        /// <summary>
        /// 获取分片列表。
        /// </summary>
        internal static BlockTokenCollection Tokens
        {
            get
            {
                if (_tokens == null)
                { _tokens = new BlockTokenCollection(); }

                return _tokens;
            }
        }

        #endregion

        #region 公共属性定义 - 分片标识信息

        /// <summary>
        /// 获取当前分片对象用于存储分片列表的对象池。
        /// </summary>
        internal BlockTokenCollection TokensPool
        {
            get { return this._tokensPool; }
        }

        /// <summary>
        /// 获取当前分片对象的 MD5 值，表示该分片的唯一标识。
        /// </summary>
        public string MD5
        {
            get { return this._md5; }
        }

        /// <summary>
        /// 获取表示当前分片的文件模型对象。
        /// <para>如果当前分片无效，则返回 null。</para>
        /// </summary>
        internal FileInfoModel User
        {
            get { return this._file; }
        }

        #endregion

        #region internal 静态方法定义 - 获取分片相关信息

        /// <summary>
        /// 获取用于存储分片列表的对象池。
        /// </summary>
        /// <returns></returns>
        internal static BlockTokenCollection GetTokensPool()
        {
            return Tokens;
        }

        #endregion

        #region 私有方法定义 - 初始化基础对象

        private void InitFile()
        {
            Check.NotEmpty(this._md5);
            this._file = new FileInfoModel {  };
        }

        #endregion
    }
}